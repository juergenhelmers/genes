require 'nokogiri'
require 'open-uri'
require 'csv'

input_file = CSV.read("/media/truecrypt1/box.net/My Box Files/TorrenZ/Technology Working Group/Ion Torrent Proton Sequencer/Ion_AmpliSeq_Inherited_Disease_Panel_list_of_target_genes.csv")



gene_name = "TYR"
chromosome_nr = "11"

# url at ncbi
url = "http://www.ncbi.nlm.nih.gov/gene?term=((homo%20sapiens%5BOrganism%5D)%20AND%20#{gene_name}%5BGene%20Name%5D)%20AND%20#{chromosome_nr}%5BChromosome%5D"
# create a nokogiri object
doc = Nokogiri::HTML(open(url))
# get the 20th dd which does contain the summary
puts doc.css("dd:nth-child(20)").text
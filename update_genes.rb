require 'csv'

# create csv object from file
input_file = CSV.read("/home/helmerj/Documents/LibreOffice/ion_torrent_genes_diseases_list.csv", :headers => true, :col_sep => ";")

# loop over file and check if gene does exist. or write into new file to be processed.

input_file.each do |line|                                                                    # loop over csv object
  record = Gene.find_or_create_by_name(line['gene'])  # use the gene name to find or create a gene record
  record.disease = line['disease']                                                  # set the disease fied to the value from csv file
  record.save                                                                                           # save file
end

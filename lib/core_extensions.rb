class String
  def to_blurb(word_count = 18)
    self.split(" ").slice(0, word_count).join(" ")
  end
end
require 'csv'

namespace :IDP do
  desc "compare illumina and life tech genes and output common genes"

  task :compare_genes => :environment do
    ThinkingSphinx.deltas_enabled = false                           # disable delta indexing during processing
    i_file = "/home/helmerj/Downloads/illumina/illumina_genes.csv"  # define the two csv file paths
    t_file = "/home/helmerj/Downloads/illumina/torrent_genes.csv"

    i_csv = CSV.read(i_file, :headers => false)                     # read the csv files
    t_csv = CSV.read(t_file, :headers => false)

    i_array = []                                                    # convert each CSV::Table to an array
    i_csv.each do |gene|
      i_array << gene[0]
    end
    t_array = []
    t_csv.each do |gene|
      t_array << gene[0]
    end

    common_array  = t_array & i_array                               # intersect the two array to find common elements
    torrent_array = t_array - i_array                               # substract the two array to find unique elements
    illumina_array = i_array - t_array                              # substract the two array to find unique elements

    # get associated disease for the genes from the database and save to a new CSV file
    CSV.open("/tmp/common_genes.csv", "w") do |csv|
       common_array.each do |gene|
         disease = Gene.find_by_name(gene).disease
         full_name = Gene.find_by_name(gene).ncbi_result.official_full_name
         line = []
         line << gene
         line << full_name
         line << disease
         csv << line
       end
    end

    # get life tech only genes

    CSV.open("/tmp/torrent_genes.csv", "w") do |csv|
       t_array.each do |gene|
         disease = Gene.find_by_name(gene).disease
         full_name = Gene.find_by_name(gene).ncbi_result.official_full_name
         line = []
         line << gene
         line << full_name
         line << disease
         csv << line
       end
    end

    # get information for illumina only genes

    require 'bio'
    require 'nokogiri'
    require 'open-uri'

    Bio::NCBI.default_email = "juergen.helmers@gmail.com"

    CSV.open("/tmp/illumina_genes_2.csv", "w", {:col_sep => ";", :quote_char => '"'}) do |csv|
      illumina_array.each do |gene|
        ncbi_gene_id = Bio::NCBI::REST.esearch("#{gene}[gene name]+homo sapiens[organism]", {"db" => "gene"})[0]
        xml = Bio::NCBI::REST.efetch("#{ncbi_gene_id}", {"db"=>"gene","retmode"=>"xml"})
        ncbi_doc = Nokogiri::XML(xml)
        summary = ncbi_doc.xpath('//Entrezgene_summary').text
        #references = ncbi_doc.xpath('//Entrezgene_gene/Gene-ref/Gene-ref_db/Dbtag')
        #references.each do |reference|
        #  ref_type = reference.xpath('Dbtag_db').text.lstrip
        #  if ref_type == "MIM"
        #    @reference_id = reference.xpath('Dbtag_tag/Object-id').text.lstrip.gsub("\n","").gsub(" ","")
        #  end
        #end
        #url = "http://www.omim.org/entry/#{@reference_id}"
        #doc = Nokogiri::HTML(open(url))
        full_name = ncbi_doc.xpath('//Entrezgene_properties/Gene-commentary/Gene-commentary_properties/Gene-commentary/Gene-commentary_text')[1].text
        #disease = doc.at_css(".gene-map-value.text-font.lookup.value-border-right").text.lstrip
        line = []
        line << gene
        line << full_name
        line << summary
        csv << line
      end
    end


  end
end
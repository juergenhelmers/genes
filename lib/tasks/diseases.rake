#Include the csv class
require 'csv'
namespace :iontorrent do
  desc "add diseases to existing genes"
  task :add_diseases => :environment do
    input_file = CSV.read('/home/helmerj/Documents/LibreOffice/ion_torrent_idp_gene_disease_list.csv', :headers => true, :col_sep => ";")
    input_file.each do |line|
      gene = Gene.find_by_name(line['Name'])
      if gene.present?
        gene.disease = line['Disease']
        gene.save
      end
    end

  end
end

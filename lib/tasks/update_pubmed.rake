#Include the bioruby and nokogiri classes
require 'bio'
require 'nokogiri'

Bio::NCBI.default_email = "juergen.helmers@gmail.com"
namespace :NCBI do
  desc "update pubmed articles for all genes"
  task :update_articles => :environment do
    ThinkingSphinx.deltas_enabled = false         # disable delta indexing during update
    gene_ids_hash = {}                                                     # instantiate an empty hash to hold id's of gene w/o a ncbi_result
    Gene.all.each_with_index do |gene,index|   # loop over all genes
      gene_ids_hash[index] = gene.id                        # save gene id in hash
    end

    Resque.enqueue(NcbiUpdateScraper, gene_ids_hash)   # enqueue the hash of ids to the resque worker

  end

  desc "update pubmed articles mesh, authors and affiliations fields"
    task :fix_array_fields => :environment do
      count =  NcbiPubmedArticle.count
      NcbiPubmedArticle.all.each_with_index  do |article, index|
         article.authors       = article.authors.strip.gsub(",","").gsub("\n-", ",").gsub("---, ","")
         article.mesh            = article.mesh.gsub("'\n- ",", ").gsub("---\n- ! '", "").gsub("\n- ",", ").gsub("! '", "").gsub("---, ","")
         article.affiliations =  article.affiliations.strip.gsub("---\n- ","").gsub("\n ","").gsub("  ", " ")
         article.save
         puts "#{index} of #{count}"
      end
    end

  desc "update pubmed articles with review flag"
    task :update_review => :environment do
      ThinkingSphinx.deltas_enabled = false                                       # disable delta indexing during update


      Gene.all.each do |gene|                                                     # loop over all genes
        article_ids_hash = {}                                                     # instantiate an empty id hash
        gene.ncbi_result.ncbi_pubmed_articles.each_with_index do |article,index|  # loop over all genes
          article_ids_hash[index] = article.id                                    # save gene id in hash
        end

        Resque.enqueue(NcbiArticleUpdateScraper, article_ids_hash)                # enqueue the hash of ids to the resque worker

      end


    end

end

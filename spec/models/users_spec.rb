# spec/models/user.rb
require 'spec_helper'

describe User do
  it "has a valid factory" do
    user = FactoryGirl.create(:user).should be_valid

  end

  it "is invalid without a login" do
    FactoryGirl.build(:user, login: nil).should_not be_valid
  end
  it "is invalid without a email" do
    FactoryGirl.build(:user, email: nil).should_not be_valid
  end
  it "is invalid without a firstname" do
    FactoryGirl.build(:user, firstname: nil).should_not be_valid
  end
  it "is invalid without a lastname" do
    FactoryGirl.build(:user, lastname: nil).should_not be_valid
  end

  it "is invalid with taken login" do
    user = FactoryGirl.create(:user, login: "taken")
    FactoryGirl.build(:user, login: "taken").should_not be_valid
    user.destroy
  end

  it "is invalid with taken email" do
    user = FactoryGirl.create(:user, email: "taken@something.com")
    FactoryGirl.build(:user, email: "taken@something.com").should_not be_valid
    user.destroy
  end

  it "should assign the default class of user" do
    role = FactoryGirl.create(:role)
    user = FactoryGirl.create(:user)
    user.role.name.should == "user"
    role.destroy
    user.destroy
  end

  #it "should create an admin user" do
  #  role = FactoryGirl.build(:admin_role)
  #  user = FactoryGirl.create(:admin_user, login: "admin" )
  #  user.role.name.should == "admin"
  #
  #end

  #it "should log in the user and get a valid session" do
  #  user = FactoryGirl.create(:admin_user)
  #  visit('/my/users/sign_in')
  #  fill_in('user_email', :with => user.email)
  #  fill_in('user_password', :with => user.password)
  #  #save_and_open_page
  #  click_button('Sign in')
  #
  #end

end
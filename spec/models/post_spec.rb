# spec/models/post.rb
require 'spec_helper'
describe Post do
  it "should have a title" do
    subject.should respond_to(:title)
  end
  it "should have a content" do
    subject.should respond_to(:content)
  end

  it "should have a valid factory" do
    FactoryGirl.build(:post).should be_valid
  end

  it "should return published if puisblished set to true" do
    post = FactoryGirl.build(:post)
    post.should be_published
  end

  it "should return unpublished if it is a draft" do
    post = FactoryGirl.build(:draft_post)
    post.should_not be_published
  end

  it "scope published should return record if published" do
    post = FactoryGirl.create(:post)
    draft = FactoryGirl.create(:draft_post)
    Post.published.map(&:title).should include(post.title)
    Post.published.map(&:title).should_not include(draft.title)
    post.destroy
    draft.destroy
  end

  it "should return unpublished if it is a draft" do
    post = FactoryGirl.build(:draft_post)
    post.should_not be_published
  end

  it "scope unpublished should return record if unpublished" do
    draft = FactoryGirl.create(:draft_post)
    post = FactoryGirl.build(:post)
    Post.unpublished.map(&:title).should include(draft.title)
    Post.unpublished.map(&:title).should_not include(post.title)
    post.destroy
    draft.destroy
  end
end

# spec/models/role.rb
require 'spec_helper'



describe Role do

  before(:each) do
    @role = FactoryGirl.build(:role)
  end

  it "has a valid factory" do
    @role.should be_valid
    @role.should have(:no).errors_on(:name)
  end

  it "should have a name" do
    subject.should respond_to(:name)
  end

  it "is invalid without a name" do
    FactoryGirl.build(:role, name: nil).should_not be_valid
  end



end
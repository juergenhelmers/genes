# spec/models/gene.rb
require 'spec_helper'

describe Gene do
  it "has a valid factory" do
    FactoryGirl.build(:gene).should be_valid
  end

  it "is invalid without a name" do
    FactoryGirl.build(:gene, name: nil).should_not be_valid
  end

  it "is invalid without a chromosome" do
    FactoryGirl.build(:gene, chromosome: nil).should_not be_valid
  end

  it "is invalid with an already existing name" do
    g = FactoryGirl.create(:gene, name: "test")
    FactoryGirl.build(:gene, name: "test").should_not be_valid
    g.destroy
  end

  it "generates proper related link urls" do
    Gene.generate_related_link("HGNC",456378).should == "http://www.genenames.org/data/hgnc_data.php?hgnc_id=456378"
  end

  it "calculates the progress" do
    progress = Gene.calculate_progress(20, 200)
    progress.should include(
      "classified" => "91",
      "unclassified" => "9"
    )
  end

  it "returns the list of unclassified genes" do
    gene = FactoryGirl.create(:gene, name: "ATXN")
    Gene.without_classification.map(&:name).should include(gene.name)
    Gene.with_classification.map(&:name).should_not include(gene.name)
    gene.destroy
  end

  #it "returns a list of classified genes if gender_classified" do
  #  gene = FactoryGirl.create(:classified_gene, name: "TYR_class")
  #  Gene.with_classification.map(&:name).should include(gene.name)
  #  Gene.without_classification.map(&:name).should_not include(gene.name)
  #  gene.destroy
  #end

  it "returns a list of common in SA genes if common_in_sa is true" do
    gene = FactoryGirl.create(:sa_gene, name: "TYR_SA")
    Gene.common_in_sa.map(&:name).should include(gene.name)
    gene.destroy
  end
end
# spec/factories/roles.rb
FactoryGirl.define do

  factory :role do |f|
    f.name "user"
    f.description "regular user with no special access rights."
    f.initialize_with { Role.find_or_create_by_name(name)}
  end

  factory :admin_role, class: Role do |f|
    f.name "admin"
    f.initialize_with { Role.find_or_create_by_name(name)}
  end

  factory :gene do |f|
    f.name "tyr"
    f.chromosome "chr11"
    f.covered_bases "3456"
    f.total_bases "1765"
    f.overall_coverage "0.998"
    f.association :ncbi_result
  end

  factory :sa_gene, class: Gene do |f|
    f.name "tyr"
    f.chromosome "chr11"
    f.covered_bases "3456"
    f.total_bases "1765"
    f.overall_coverage "0.998"
    f.common_in_sa true
    f.association :ncbi_result
  end

  factory :classified_gene, class: Gene do |f|
    f.name "tyr"
    f.chromosome "chr11"
    f.covered_bases "3456"
    f.total_bases "1765"
    f.overall_coverage "0.998"
    f.association :chromosome_classification
    f.association :ncbi_result
  end

  factory :invalid_gene, parent: :gene do |f|
    f.name nil
    f.chromosome "chrX"
  end

  factory :user do
    sequence(:login) { |n| "foo#{n}" }
    email { "#{login}@something.com" }
    sequence(:firstname) {|n| "John#{n}"}
    lastname "Doe"
    password "secret"
    active true
    association :role
  end

  factory :inactive_user, class: User do
    sequence(:login) { |n| "foo#{n}" }
    email { "#{login}@something.com" }
    sequence(:firstname) {|n| "John#{n}"}
    lastname "Doe"
    password "secret"
    active false
  end

  factory :invalid_user, class: User do
    sequence(:login) { |n| "foo#{n}" }
    email { "#{login}@something.com" }
    sequence(:firstname) {|n| "John#{n}"}
    lastname nil
    password "secret"
    active true
  end

  factory :admin_user, class: User do
    sequence(:login) { |n| "foo#{n}" }
    email { "#{login}@something.com" }
    firstname "John"
    lastname "Doe"
    password "secret"
    active true
    association :role, factory: :admin_role
  end

end

# spec/factories/ncbi_result.rb
FactoryGirl.define do
  factory :ncbi_result , :class => NcbiResult do |f|
    f.official_full_name "some full name"
    f.official_symbol "TYR"
    f.organism "Homo Sapiens"
    f.ncbi_gene_id "324234"
    f.ncbi_related_links { |rl| [rl.association(:ncbi_related_link)] }
    f.ncbi_gene_type
  end
end

FactoryGirl.define do
  factory :post do |f|
    f.title "title for published post"
    f.content "content for this post"
    f.association :user
    published true
  end

  factory :draft_post, class: Post do |f|
    f.title "title for unpublished post"
    f.content "content for this post"
    f.association :user
    published false
  end

end

FactoryGirl.define do
  factory :ncbi_related_link do |f|
    f.related_link_type "HGNC"
    f.sequence(:related_link_id) { |n| "234234#{n}"}
  end
end

FactoryGirl.define do
  factory :ncbi_gene_type do |f|
    f.gene_type_name "protein-coding"
  end
end

# Classifications
FactoryGirl.define do
  factory :chromosome do
    name "autosomal"
  end
end

FactoryGirl.define do
  factory :chromosome_classification do |f|
    f.association :chromosome
  end
end

FactoryGirl.define do
  factory :dominance do
    name "MyString"
  end
end

FactoryGirl.define do
  factory :dominance_classification do
    gene_id 1
    dominance_id 1
    delta false
    user_id 1
  end
end

FactoryGirl.define do
  factory :gender do
    name "female"
  end
end

FactoryGirl.define do
  factory :gender_classification do
    gene_id 1
    gender_id 1
    delta false
    user_id 1
  end
end

FactoryGirl.define do
  factory :zygosity do
    name "MyString"
  end
end


FactoryGirl.define do
  factory :zygosity_classification do
    gene_id 1
    zygosity_id 1
    delta false
    user_id 1
  end
end


require 'spec_helper'

describe "Users as regular user" do

  before(:each) do
    @user = FactoryGirl.create(:user)
    visit('/my/users/sign_in')
    fill_in 'user_email', :with => @user.email
    fill_in 'user_password', :with => @user.password
    click_button('Sign in')
    current_path.should eq('/')
  end

  describe "GET /users as regular user" do
    it "should list the current user" do
      visit(users_path)
      current_path.should eq(users_path)
      page.should have_content("#{@user.firstname}")
      page.should have_content("#{@user.lastname}")
    end

    it "should only list the current user" do
      visit(users_path)
      current_path.should eq(users_path)
      User.all.should have(1).items
    end

    it "should show the edit user button" do
      visit(users_path)
      current_path.should eq(users_path)
      page.should have_content("Edit")

    end

    it "should NOT show the destroy user button" do
      visit(users_path)
      current_path.should eq(users_path)
      page.should_not have_content("Destroy")

    end

    it "should NOT show the new user form when visiting new_user_path" do
      visit(new_user_path)
      current_path.should eq("/")
      page.should have_content("Not allowed to manage other user accounts.")
    end

    it "should NOT show the current user another users profile" do
      user = FactoryGirl.create(:user)
      visit("/users/#{user.id}")
      current_path.should eq("/users/#{user.id}")
      page.should have_content(@user.firstname)
      page.should_not have_content(user.firstname)
      user.destroy
    end

  end

end


describe "User as admin users" do
  before(:each) do
    @user = FactoryGirl.create(:admin_user)
    visit('/my/users/sign_in')
    fill_in 'user_email', :with => @user.email
    fill_in 'user_password', :with => @user.password
    click_button('Sign in')
    current_path.should eq('/')
  end

  describe "GET /users as admin user" do

    it 'should list all users' do
      user1 = FactoryGirl.create(:user)
      user2 = FactoryGirl.create(:user)
      user3 = FactoryGirl.create(:user)
      visit(users_path)
      current_path.should eq(users_path)
      page.should have_content(user1.firstname)
      page.should have_content(user2.firstname)
      page.should have_content(user3.firstname)
      user1.destroy
      user2.destroy
      user3.destroy
    end

    it "should have edit buttons for users" do
      user = FactoryGirl.create(:user)
      visit(users_path)
      current_path.should eq(users_path)
      page.should have_content("Edit")
      user.destroy
    end

    it "should have destroy buttons for users" do
      user = FactoryGirl.create(:user)
      visit(users_path)
      current_path.should eq(users_path)
      page.should have_content("Delete")
    end

    it "should show the new user form when visiting new_user_path" do
      visit(new_user_path)
      current_path.should eq(new_user_path)
      page.should have_content("New User")
    end

    it "should NOT show the current user another users profile" do
      user = FactoryGirl.create(:user)
      visit("/users/#{user.id}")
      current_path.should eq("/users/#{user.id}")
      page.should have_content(user.firstname)
      user.destroy
    end


  end


end

describe "inactive user" do
  it "should not be able to log into the system" do
    user = FactoryGirl.create(:inactive_user)
    visit('/my/users/sign_in')

    fill_in 'user_email', :with => user.email
    fill_in 'user_password', :with => user.password
    click_button('Sign in')
    current_path.should eq('/my/users/sign_in')
    page.should have_content('Your account has been deactivated. Please contact the administrator.')
    user.destroy
  end

  it "should not be able to see genes index view" do
    visit(genes_path)
    current_path.should eq('/my/users/sign_in')
    page.should have_content('You need to sign in or sign up before continuing.')
  end
  it "should not be able to see users index view" do
    visit(users_path)
    current_path.should eq('/my/users/sign_in')
    page.should have_content('You need to sign in or sign up before continuing.')
  end
  it "should not be able to see roles index view" do
    visit(roles_path)
    current_path.should eq('/my/users/sign_in')
    page.should have_content('You need to sign in or sign up before continuing.')
  end

end

require 'spec_helper'

describe "Genes" do

  before(:each) do
    @user = FactoryGirl.create(:user)
    visit('/my/users/sign_in')
    fill_in 'user_email', :with => @user.email
    fill_in 'user_password', :with => @user.password
    click_button('Sign in')
    current_path.should eq('/')
  end

  describe "GET /genes as regular user" do
    it "should list a gene in the index" do
      gene = FactoryGirl.create(:gene)
      visit(genes_path)
      current_path.should eq(genes_path)
      #save_and_open_page
      page.should have_content("Some Full Name")

    end

    it "should NOT show the new gene form when visiting new_gene_path for regular user" do
      gene = FactoryGirl.create(:gene)
      visit(new_gene_path)
      current_path.should eq("/")
      page.should have_content("You are not authorized to add new genes.")
      gene.destroy
    end

    it "should list gene details when visiting show page" do
      gene = FactoryGirl.create(:gene)
      visit("/genes/#{gene.id}")
      page.should have_content("Some Full Name")
      gene.destroy
    end

    it "should render gene update form when visiting update page" do
      gene = FactoryGirl.create(:gene)
      visit("/genes/#{gene.id}/edit")
      page.should have_content("You are not authorized to edit this gene.")
      gene.destroy
    end

  end
end

describe "Genes" do
  before(:each) do
     @user = FactoryGirl.create(:admin_user)

     visit('/my/users/sign_in')
     fill_in('user_email', :with => @user.email)
     fill_in('user_password', :with => @user.password)
     click_button('Sign in')
     current_path.should eq('/')
  end

  describe "GET /genes as admin user" do
    it "should list a gene in the index" do
      gene = FactoryGirl.create(:gene)
      visit(genes_path)
      current_path.should eq(genes_path)
      page.should have_content("Some Full Name")
      gene.destroy
    end

    it "should NOT show the new gene form when visiting new_gene_path for admin user" do
      visit(new_gene_path)
      current_path.should eq(new_gene_path)
      page.should have_content("Genes")
      page.should have_content("Mutation annotation file")
    end

    it "should list gene details when visiting show page" do
      gene = FactoryGirl.create(:gene)
      visit("/genes/#{gene.id}")
      page.should have_content("Some Full Name")
      page.should have_content("Edit")
      gene.destroy
    end

    it "should render gene update form when visiting update page" do
      gene = FactoryGirl.create(:gene)
      visit("/genes/#{gene.id}/edit")
      page.should have_content("Edit Gene #{gene.name}")
      gene.destroy
    end

  end
end

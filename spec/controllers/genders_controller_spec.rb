require 'spec_helper'


describe GendersController do

  before(:each) do
    @user = FactoryGirl.create(:admin_user)
    sign_in @user

  end

  context "create a new gender" do

    it "creates a new Gender" do
      expect {
        post :create, gender: { :name => "gender name"}
      }.to change(Gender,:count).by(1)
      Gender.all.each{|r| r.destroy}
    end

    it "should redirect to the new gender show view with notice" do
      post :create, gender: { :name => "gender name"}
      response.should redirect_to Gender.last
      Gender.all.each{ |r| r.destroy }
    end
  end

  context "should not create a new gender" do
    it "should not create a new gender with no name" do
      expect{
        post :create, gender: { :name => nil }
      }.to_not change(Gender,:count)
    end

    it "re-render the form upon failed save" do
      post :create, gender: { :name => nil }
      response.should redirect_to new_gender_path
    end

  end

  context "GET #index" do
    it "populates an array of genders"do
      gender = FactoryGirl.create(:gender)
      get :index
      assigns(:genders).should include(gender)
      gender.destroy
    end
    it "renders the :index view" do
      get :index
      response.should render_template :index
    end
  end

  context "GET # edit" do
    it "should find gender to edit" do
      gender = FactoryGirl.create(:gender)
      get :edit, id: gender
      assigns(:gender).should eq(gender)
      gender.destroy
    end

  end

  context "GET #show" do
    it "assigns the requested gender to @gender" do
      gender = FactoryGirl.create(:gender)
      get :show, id: gender
      assigns(:gender).should eq(gender)
      gender.destroy
    end
    it "renders the :show template" do
      gender = FactoryGirl.create(:gender)
      get :show, id: gender
      response.should render_template :show
      gender.destroy
    end
  end

  context "GET #new" do
    it "assigns a new Gender to @gender" do
      get :new
      assigns(:gender).should be_a_new(Gender)
    end


    it "renders the :new template" do
      get :new
      response.should render_template :new
    end
  end

  context "put #update" do
    it "should render the edit template" do
      gender = FactoryGirl.create(:gender)
      get :edit, id: gender
      response.should render_template :edit
      gender.destroy
    end

    it "should assign the update gender to @gender" do
      gender = FactoryGirl.create(:gender)
      get :edit, id: gender
      assigns(:gender).should eq(gender)
      gender.destroy
    end

  end



end
#require 'spec_helper'
#
#
#describe EthnicGroupsController do
#
#  before(:each) do
#    @user = FactoryGirl.create(:admin_user)
#    sign_in @user
#
#  end
#
#  context "create a new ethnic_group" do
#
#    it "creates a new EthnicGroup" do
#      expect {
#        post :create, ethnic_group: { :value => "ethnic_group value"}
#      }.to change(EthnicGroup,:count).by(1)
#      EthnicGroup.all.each{|r| r.destroy}
#    end
#
#    it "should redirect to the new ethnic_group show view with notice" do
#      post :create, ethnic_group: { :value => "ethnic_group value"}
#      response.should redirect_to EthnicGroup.last
#      EthnicGroup.all.each{ |r| r.destroy }
#    end
#  end
#
#  context "should not create a new ethnic_group" do
#    it "should not create a new ethnic_group with no value" do
#      expect{
#        post :create, ethnic_group: { :value => nil }
#      }.to_not change(EthnicGroup,:count)
#    end
#
#    it "re-render the form upon failed save" do
#      post :create, ethnic_group: { :value => nil }
#      response.should redirect_to new_ethnic_group_path
#    end
#
#  end
#
#  context "GET #index" do
#    it "populates an array of ethnic_groups"do
#      ethnic_group = FactoryGirl.create(:ethnic_group)
#      get :index
#      assigns(:ethnic_groups).should include(ethnic_group)
#      ethnic_group.destroy
#    end
#    it "renders the :index view" do
#      get :index
#      response.should render_template :index
#    end
#  end
#
#  context "GET # edit" do
#    it "should find ethnic_group to edit" do
#      ethnic_group = FactoryGirl.create(:ethnic_group)
#      get :edit, id: ethnic_group
#      assigns(:ethnic_group).should eq(ethnic_group)
#      ethnic_group.destroy
#    end
#
#  end
#
#  context "GET #show" do
#    it "assigns the requested ethnic_group to @ethnic_group" do
#      ethnic_group = FactoryGirl.create(:ethnic_group)
#      get :show, id: ethnic_group
#      assigns(:ethnic_group).should eq(ethnic_group)
#      ethnic_group.destroy
#    end
#    it "renders the :show template" do
#      ethnic_group = FactoryGirl.create(:ethnic_group)
#      get :show, id: ethnic_group
#      response.should render_template :show
#      ethnic_group.destroy
#    end
#  end
#
#  context "GET #new" do
#    it "assigns a new EthnicGroup to @ethnic_group" do
#      get :new
#      assigns(:ethnic_group).should be_a_new(EthnicGroup)
#    end
#
#
#    it "renders the :new template" do
#      get :new
#      response.should render_template :new
#    end
#  end
#
#  context "put #update" do
#    it "should render the edit template" do
#      ethnic_group = FactoryGirl.create(:ethnic_group)
#      get :edit, id: ethnic_group
#      response.should render_template :edit
#      ethnic_group.destroy
#    end
#
#    it "should assign the update ethnic_group to @ethnic_group" do
#      ethnic_group = FactoryGirl.create(:ethnic_group)
#      get :edit, id: ethnic_group
#      assigns(:ethnic_group).should eq(ethnic_group)
#      ethnic_group.destroy
#    end
#
#  end
#
#
#
#end
require 'spec_helper'


describe UsersController do

  before(:each) do
    @user = FactoryGirl.create(:admin_user)
    sign_in @user
  end

  context "create a new user" do

    it "creates a new User" do
      expect {
        post :create, user: FactoryGirl.attributes_for(:user)
      }.to change(User,:count).by(1)
      User.all.each { |u| u.destroy }
    end

    it "should redirect to the new user page after save" do
      post :create, user: FactoryGirl.attributes_for(:user)
      response.should redirect_to User.last
      User.all.each { |u| u.destroy }
    end
  end

  context "does not create a new user" do
    it "creates a new User" do
      expect {
        post :create, user: FactoryGirl.attributes_for(:invalid_user)
      }.to_not change(User,:count)
      User.all.each { |u| u.destroy }
    end

    it "should re-render the form upon failed save" do
      post :create, user: FactoryGirl.attributes_for(:invalid_user)
      response.should redirect_to new_user_path
    end

  end
end
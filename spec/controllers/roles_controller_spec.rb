require 'spec_helper'


describe RolesController do

  before(:each) do
    @user = FactoryGirl.create(:admin_user)
    sign_in @user

  end

  context "create a new role" do

    it "creates a new Role" do
      expect {
        post :create, role: { :name => "role name"}
      }.to change(Role,:count).by(1)
      Role.all.each{|r| r.destroy}
    end

    it "should redirect to the new role show view with notice" do
      post :create, role: { :name => "role name"}
      response.should redirect_to Role.last
      Role.all.each{ |r| r.destroy }
    end
  end

  context "should not create a new role" do
    it "should not create a new role with no name" do
      expect{
        post :create, role: { :name => nil }
      }.to_not change(Role,:count)
    end

    it "re-render the form upon failed save" do
      post :create, role: { :name => nil }
      response.should redirect_to new_role_path
    end

  end

  context "GET #index" do
    it "populates an array of roles"do
      role = FactoryGirl.create(:role)
      get :index
      assigns(:roles).should include(role)
      role.destroy
    end
    it "renders the :index view" do
      get :index
      response.should render_template :index
    end
  end

  context "GET # edit" do
    it "should find role to edit" do
      role = FactoryGirl.create(:role)
      get :edit, id: role
      assigns(:role).should eq(role)
      role.destroy
    end

  end

  context "GET #show" do
    it "assigns the requested role to @role" do
      role = FactoryGirl.create(:role)
      get :show, id: role
      assigns(:role).should eq(role)
      role.destroy
    end
    it "renders the :show template" do
      role = FactoryGirl.create(:role)
      get :show, id: role
      response.should render_template :show
      role.destroy
    end
  end

  context "GET #new" do
    it "assigns a new Role to @role" do
      get :new
      assigns(:role).should be_a_new(Role)
    end


    it "renders the :new template" do
      get :new
      response.should render_template :new
    end
  end

  context "put #update" do
    it "should render the edit template" do
      role = FactoryGirl.create(:role)
      get :edit, id: role
      response.should render_template :edit
      role.destroy
    end

    it "should assign the update role to @role" do
      role = FactoryGirl.create(:role)
      get :edit, id: role
      assigns(:role).should eq(role)
      role.destroy
    end

  end



end
require 'spec_helper'


describe GenesController do

  before(:each) do
    @user = FactoryGirl.create(:admin_user)
    sign_in @user
  end

  def valid_attributes
    tempfile = "/home/helmers/rails/genes/tmp/test_input_single_gene.csv"
  end

  describe "GET #index" do
    it "populates an array of genes"do
      gene = FactoryGirl.create(:gene)
      get :index
      assigns(:genes).should eq([gene])

    end
    it "renders the :index view" do
      get :index
      response.should render_template :index
    end
  end

  describe "GET # edit" do
    it "should find gene to edit" do
      gene = FactoryGirl.create(:gene)
      get :edit, id: gene
      assigns(:gene).should eq(gene)

    end

  end

  describe "GET #show" do
    it "assigns the requested gene to @gene" do
      gene = FactoryGirl.create(:gene)
      get :show, id: gene
      assigns(:gene).should eq(gene)

    end
    it "renders the :show template" do
      gene = FactoryGirl.create(:gene)
      get :show, id: gene
      response.should render_template :show

    end
  end

  describe "GET #new" do
    it "assigns a new Gene to @gene" do
      get :new
      assigns(:gene).should be_a_new(Gene)
    end


    it "renders the :new template" do
      get :new
      response.should render_template :new
    end
  end

  describe "put #update" do
    it "should render the edit template" do
      gene = FactoryGirl.create(:gene)
      get :edit, id: gene
      response.should render_template :edit

    end

    it "should assign the update gene to @gene" do
      gene = FactoryGirl.create(:gene)
      get :edit, id: gene
      assigns(:gene).should eq(gene)

    end

  end

  describe "POST #create" do
    context "with valid attributes" do
      @gene = FactoryGirl.build(:gene)
      it "saves the new gene in the database" do
        put :new, :gene => @gene
        gene = Gene.last
        gene.should eq(@gene)
      end

      it "redirects to the home page"
    end

    context "with invalid attributes" do
      it "does not save the new gene in the database"
      it "re-renders the :new template"
    end
  end

end

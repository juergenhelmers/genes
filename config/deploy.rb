#RVM setup
require "rvm/capistrano"                              # Load RVM's capistrano plugin.
require 'thinking_sphinx/deploy/capistrano'           # load thinkink-sphinx plugin
set :rvm_ruby_string, 'ruby-1.9.3-p194@genes'         # use 1.9.2@gemsetname for using RVM gemset
set :rvm_type, :user

# server details
set :application, "genes"
set :deploy_to, "/backup/www-rails/capistrano/#{application}"
default_run_options[:pty] = true
ssh_options[:forward_agent] = true
set :deploy_via, :remote_cache
set :keep_releases, 4

set :user, "passcaprub19rs3"
set :use_sudo, false
role :web, "46.38.235.18"                             # Your HTTP server, Apache/etc
role :app, "46.38.235.18"                             # This may be the same as your `Web` server
role :db,  "46.38.235.18", :primary => true           # This is where Rails migrations will run

# repo details
set :scm, :git
set :scm_username, "passcaprub19rs3"
set :repository, "keiblinger.com:/backup/git/mutations"
set :branch, "master"
#set :git_enable_submodules, 1

# hooks
#  update gems
after   "deploy:update_code",       "bundle_gems"
#  migrate database
after   "deploy:update_code",       "deploy:create_dbconfig"
after   "deploy:update_code",       "deploy:migrate"

# thinking-sphinx callbacks
before   "deploy:migrate",           "ts:sphinx_config"
after   "ts:sphinx_config",         "ts:ready"
before  "deploy:rollback:revision", "ts:stop_gracefully"
after   "deploy:update_code",       "deploy:pipeline_precompile"

#clean up old releases
after "deploy:update", "deploy:cleanup"

 namespace :deploy do
   task :start do ; end
   task :stop do ; end
   task :create_dbconfig, :roles => :app do
     run "ln -fs #{File.join(shared_path, 'config','database.yml.deploy')} #{File.join(release_path,'config','database.yml')}"
   end


   task :restart, :roles => :app, :except => { :no_release => true } do
     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
   end

   task :pipeline_precompile do
     run "cd #{release_path}; RAILS_ENV=production bundle exec rake assets:precompile"
   end

   task :migrate do
     run "cd #{release_path}; RAILS_ENV=production bundle exec rake db:migrate"
   end

 end

namespace :ts do
  desc "Create Sphinx config links"
  task :sphinx_config, :roles => :app do
    run "ln -fs #{File.join(shared_path,'config', 'sphinx.yml.deploy')} #{File.join(release_path,'config','sphinx.yml')}"
    run "ln -fs #{File.join(shared_path,'db', 'sphinx')} #{File.join(release_path,'db','sphinx')}"
  end

  task :stop_gracefully do
    begin
      thinking_sphinx.stop
    rescue
      puts "ThinkingSphinx is not running. No stop required."
    end
  end

  desc "update index"
  task :ready do
    run "cd #{release_path}; RAILS_ENV=production bundle exec rake ts:config"
    run "cd #{release_path}; RAILS_ENV=production bundle exec rake ts:rebuild"
  end

  desc "recreate the sphinx index"
  task :reindex, :roles => :app do
    run "cd #{current_path}; RAILS_ENV=production bundle exec rake ts:index"
  end

  desc "recreate the sphinx index"
  task :rebuild, :roles => :app do
    run "cd #{current_path}; RAILS_ENV=production bundle exec rake ts:rebuild"
  end
end
 
task :bundle_gems, :roles => :app do
  run "mkdir -p #{shared_path}/bundle && ln -s #{shared_path}/bundle #{release_path}/vendor/bundle"
  run "cd #{latest_release}; bundle install --deployment --without development test"
end
 
desc "List used application server libraries"
task :list_gems, :roles => :app do
  run "gem list"
end

# README #

Application for the curation of gene data.

### Abstract ###

Ruby on Rails application that imports metadata from NCBI, including

* publications
* genetic information
* known mutations
* other disorder relevant data

Authorized users have the ability to further curate the gene data by adding additional meta-data:

* mutation rates
* disorder causality
* gender & ethnic affliction
* age of onset
* outcome prognosis
* Zygosity
* etc....


### Who do I talk to? ###

* Repo owner or admin
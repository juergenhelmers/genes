# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130226153528) do

  create_table "age_of_onsets", :force => true do |t|
    t.string   "value"
    t.boolean  "delta",      :default => true, :null => false
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  add_index "age_of_onsets", ["id"], :name => "index_age_of_onsets_on_id", :unique => true
  add_index "age_of_onsets", ["value"], :name => "index_age_of_onsets_on_value", :unique => true

  create_table "age_onsetizations", :force => true do |t|
    t.integer  "gene_id"
    t.integer  "age_of_onset_id"
    t.boolean  "delta",           :default => true, :null => false
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
  end

  add_index "age_onsetizations", ["age_of_onset_id"], :name => "index_age_onsetizations_on_age_of_onset_id"
  add_index "age_onsetizations", ["gene_id"], :name => "index_age_onsetizations_on_gene_id"

  create_table "causalities", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "causalities", ["id"], :name => "index_causalities_on_id", :unique => true
  add_index "causalities", ["name"], :name => "index_causalities_on_name"

  create_table "causalitizations", :force => true do |t|
    t.integer  "gene_id"
    t.integer  "causality_id"
    t.boolean  "delta",        :default => true, :null => false
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
  end

  add_index "causalitizations", ["causality_id"], :name => "index_causalitizations_on_causality_id"
  add_index "causalitizations", ["gene_id"], :name => "index_causalitizations_on_gene_id"

  create_table "chromosome_classifications", :force => true do |t|
    t.integer  "gene_id"
    t.integer  "chromosome_id"
    t.boolean  "delta"
    t.integer  "user_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "chromosome_classifications", ["chromosome_id"], :name => "index_chromosome_classifications_on_chromosome_id"
  add_index "chromosome_classifications", ["gene_id"], :name => "index_chromosome_classifications_on_gene_id"

  create_table "chromosomes", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "chromosomes", ["id"], :name => "index_chromosomes_on_id"

  create_table "curabilities", :force => true do |t|
    t.string   "value"
    t.boolean  "delta",      :default => true, :null => false
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  add_index "curabilities", ["id"], :name => "index_curabilities_on_id", :unique => true
  add_index "curabilities", ["value"], :name => "index_curabilities_on_value", :unique => true

  create_table "curabilitizations", :force => true do |t|
    t.integer  "gene_id"
    t.integer  "curability_id"
    t.boolean  "delta",         :default => true, :null => false
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  add_index "curabilitizations", ["curability_id"], :name => "index_curabilitizations_on_curability_id"
  add_index "curabilitizations", ["gene_id"], :name => "index_curabilitizations_on_gene_id"

  create_table "dominance_classifications", :force => true do |t|
    t.integer  "gene_id"
    t.integer  "dominance_id"
    t.boolean  "delta"
    t.integer  "user_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "dominance_classifications", ["dominance_id"], :name => "index_dominance_classifications_on_dominance_id"
  add_index "dominance_classifications", ["gene_id"], :name => "index_dominance_classifications_on_gene_id"

  create_table "dominances", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "dominances", ["id"], :name => "index_dominances_on_id"

  create_table "ethnic_groupings", :force => true do |t|
    t.integer  "gene_id"
    t.integer  "ethnic_group_id"
    t.boolean  "delta",           :default => true, :null => false
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
  end

  add_index "ethnic_groupings", ["ethnic_group_id"], :name => "index_ethnic_groupings_on_ethnic_group_id"
  add_index "ethnic_groupings", ["gene_id"], :name => "index_ethnic_groupings_on_gene_id"

  create_table "ethnic_groups", :force => true do |t|
    t.string   "value"
    t.boolean  "delta",      :default => true, :null => false
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  add_index "ethnic_groups", ["id"], :name => "index_ethnic_groups_on_id", :unique => true
  add_index "ethnic_groups", ["value"], :name => "index_ethnic_groups_on_value", :unique => true

  create_table "gender_classifications", :force => true do |t|
    t.integer  "gene_id"
    t.integer  "gender_id"
    t.boolean  "delta"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "gender_classifications", ["gender_id"], :name => "index_gender_classifications_on_gender_id"
  add_index "gender_classifications", ["gene_id"], :name => "index_gender_classifications_on_gene_id"

  create_table "genders", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "genders", ["id"], :name => "index_genders_on_id"

  create_table "genes", :force => true do |t|
    t.string   "name"
    t.string   "chromosome"
    t.integer  "number_amplicons"
    t.integer  "total_bases"
    t.integer  "covered_bases"
    t.float    "overall_coverage"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.boolean  "delta",            :default => true,  :null => false
    t.string   "disease"
    t.boolean  "common_in_sa",     :default => false
  end

  add_index "genes", ["disease"], :name => "index_genes_on_disease"

  create_table "genetic_markers", :force => true do |t|
    t.integer  "unists_id"
    t.string   "name"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
    t.boolean  "delta",      :default => true, :null => false
  end

  create_table "genetic_markers_ncbi_results_joins", :force => true do |t|
    t.integer  "genetic_marker_id"
    t.integer  "ncbi_result_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "ncbi_aliases", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
    t.boolean  "delta",      :default => true, :null => false
  end

  create_table "ncbi_aliases_ncbi_results", :id => false, :force => true do |t|
    t.integer "ncbi_alias_id"
    t.integer "ncbi_result_id"
  end

  create_table "ncbi_gene_types", :force => true do |t|
    t.string   "gene_type_name"
    t.text     "gene_type_description"
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
    t.boolean  "delta",                 :default => true, :null => false
  end

  create_table "ncbi_genomic_contexts", :force => true do |t|
    t.string   "location"
    t.string   "sequence"
    t.string   "epigenomics_url"
    t.string   "mapviewer_url"
    t.integer  "ncbi_result_id"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.boolean  "delta",           :default => true, :null => false
  end

  create_table "ncbi_lineages", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.boolean  "delta",       :default => true, :null => false
  end

  create_table "ncbi_lineages_ncbi_results", :id => false, :force => true do |t|
    t.integer "ncbi_lineage_id"
    t.integer "ncbi_result_id"
  end

  create_table "ncbi_pubmed_articles", :force => true do |t|
    t.text     "authors"
    t.text     "title"
    t.string   "journal"
    t.string   "volume"
    t.string   "issue"
    t.string   "pages"
    t.string   "year"
    t.string   "pubmed"
    t.string   "medline"
    t.string   "doi"
    t.text     "abstract"
    t.string   "url"
    t.text     "mesh"
    t.string   "embl_gb_record_number"
    t.string   "sequence_position"
    t.text     "comments"
    t.text     "affiliations"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.boolean  "delta",                 :default => true,  :null => false
    t.boolean  "review",                :default => false
  end

  create_table "ncbi_related_links", :force => true do |t|
    t.string   "related_link_type"
    t.string   "related_link_id"
    t.integer  "ncbi_result_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "ncbi_result_ncbi_alias_joins", :force => true do |t|
    t.integer  "ncbi_result_id"
    t.integer  "ncbi_alias_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "ncbi_result_ncbi_lineage_joins", :force => true do |t|
    t.integer  "ncbi_result_id"
    t.integer  "ncbi_lineage_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "ncbi_result_ncbi_pubmed_article_joins", :force => true do |t|
    t.integer  "ncbi_result_id"
    t.integer  "ncbi_pubmed_article_id"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "ncbi_results", :force => true do |t|
    t.integer  "gene_id"
    t.string   "official_symbol"
    t.string   "official_full_name"
    t.string   "primary_source_link"
    t.string   "refseq_status"
    t.string   "organism"
    t.text     "summary"
    t.integer  "ncbi_gene_type_id"
    t.integer  "ncbi_gene_id"
    t.date     "ncbi_updated_at"
    t.date     "ncbi_created_at"
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
    t.string   "phenotype"
    t.boolean  "delta",               :default => true, :null => false
  end

  create_table "portals", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "posts", :force => true do |t|
    t.string   "title"
    t.text     "content"
    t.integer  "user_id"
    t.boolean  "published"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "posts", ["id"], :name => "index_posts_on_id", :unique => true

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "taggings", :force => true do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       :limit => 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id"], :name => "index_taggings_on_tag_id"
  add_index "taggings", ["taggable_id", "taggable_type", "context"], :name => "index_taggings_on_taggable_id_and_taggable_type_and_context"

  create_table "tags", :force => true do |t|
    t.string "name"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                                     :null => false
    t.string   "encrypted_password",                        :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "login",                                     :null => false
    t.string   "firstname",                                 :null => false
    t.string   "lastname",                                  :null => false
    t.integer  "role_id"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.boolean  "active",                 :default => false, :null => false
  end

  add_index "users", ["active"], :name => "index_users_on_active"
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "zygosities", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "zygosities", ["id"], :name => "index_zygosities_on_id"

  create_table "zygosity_classifications", :force => true do |t|
    t.integer  "gene_id"
    t.integer  "zygosity_id"
    t.boolean  "delta"
    t.integer  "user_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "zygosity_classifications", ["gene_id"], :name => "index_zygosity_classifications_on_gene_id"
  add_index "zygosity_classifications", ["zygosity_id"], :name => "index_zygosity_classifications_on_zygosity_id"

end

class CreateNcbiPubmedArticles < ActiveRecord::Migration
  def change
    create_table :ncbi_pubmed_articles do |t|
      t.text :authors
      t.text :title
      t.string :journal
      t.string :volume
      t.string :issue
      t.string :pages
      t.string :year
      t.string :pubmed
      t.string :medline
      t.string :doi
      t.text :abstract
      t.string :url
      t.text :mesh
      t.string :embl_gb_record_number
      t.string :sequence_position
      t.text :comments
      t.text :affiliations

      t.timestamps
    end
    add_index :ncbi_pubmed_articles, :id, :unique => true
    add_index :ncbi_pubmed_articles, :title
    add_index :ncbi_pubmed_articles, :authors
  end
end

class CreateCurabilitizations < ActiveRecord::Migration
  def change
    create_table :curabilitizations do |t|
      t.integer :gene_id
      t.integer :curability_id
      t.boolean :delta, :default => true, :null => false

      t.timestamps
    end
    add_index :curabilitizations, :gene_id
    add_index :curabilitizations, :curability_id
  end
end

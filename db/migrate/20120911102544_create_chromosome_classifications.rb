class CreateChromosomeClassifications < ActiveRecord::Migration
  def change
    create_table :chromosome_classifications do |t|
      t.integer :gene_id
      t.integer :chromosome_id
      t.boolean :delta
      t.integer :user_id

      t.timestamps
    end
    add_index :chromosome_classifications, :gene_id
    add_index :chromosome_classifications, :chromosome_id
  end
end

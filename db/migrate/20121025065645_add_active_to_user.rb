class AddActiveToUser < ActiveRecord::Migration
  def change
    add_column :users, :active, :boolean, :default => false, :null => false
    add_index :users, :active
  end
end

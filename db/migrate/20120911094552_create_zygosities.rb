class CreateZygosities < ActiveRecord::Migration
  def change
    create_table :zygosities do |t|
      t.string :name

      t.timestamps
    end
    add_index :zygosities, :id
  end
end

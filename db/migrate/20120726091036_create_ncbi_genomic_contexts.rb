class CreateNcbiGenomicContexts < ActiveRecord::Migration
  def change
    create_table :ncbi_genomic_contexts do |t|
      t.string :location
      t.string :sequence
      t.string :epigenomics_url
      t.string :mapviewer_url
      t.integer :ncbi_result_id

      t.timestamps
    end
    add_index :ncbi_genomic_contexts, :id,            :unique => true
    add_index :ncbi_genomic_contexts, :ncbi_result_id
  end
end

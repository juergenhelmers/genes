class CreateNcbiRelatedLinks < ActiveRecord::Migration
  def change
    create_table :ncbi_related_links do |t|
      t.string :related_link_type
      t.string :related_link_id
      t.integer :ncbi_result_id

      t.timestamps
    end
    add_index :ncbi_related_links, :id, :unique => true
    add_index :ncbi_related_links, :ncbi_result_id
  end
end

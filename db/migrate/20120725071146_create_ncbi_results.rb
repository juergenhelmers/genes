class CreateNcbiResults < ActiveRecord::Migration
  def change
    create_table :ncbi_results do |t|
      t.integer :gene_id
      t.string :official_symbol
      t.string :official_full_name
      t.string :primary_source_link
      t.string :refseq_status
      t.string :organism
      t.string :phenotype
      t.text   :summary
      t.integer :ncbi_gene_type_id
      t.integer :ncbi_gene_id
      t.date :ncbi_updated_at
      t.date :ncbi_created_at



      t.timestamps



    end

    add_index :ncbi_results, :id,                      :unique => true
    add_index :ncbi_results, :gene_id,                 :unique => true
  end
end

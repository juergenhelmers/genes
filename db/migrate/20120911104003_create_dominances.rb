class CreateDominances < ActiveRecord::Migration
  def change
    create_table :dominances do |t|
      t.string :name

      t.timestamps
    end
    add_index :dominances, :id
  end
end

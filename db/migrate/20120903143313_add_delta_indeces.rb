class AddDeltaIndeces < ActiveRecord::Migration
  def up
    add_column :genes,                  :delta, :boolean, :default => true, :null => false
    add_column :ncbi_results,           :delta, :boolean, :default => true, :null => false
    add_column :genetic_markers,        :delta, :boolean, :default => true, :null => false
    add_column :ncbi_gene_types,        :delta, :boolean, :default => true, :null => false
    add_column :ncbi_aliases,           :delta, :boolean, :default => true, :null => false
    add_column :ncbi_lineages,          :delta, :boolean, :default => true, :null => false
    add_column :ncbi_genomic_contexts,  :delta, :boolean, :default => true, :null => false
    add_column :ncbi_pubmed_articles,   :delta, :boolean, :default => true, :null => false
  end

  def down
    remove_column :genes,                  :delta
    remove_column :ncbi_results,           :delta
    remove_column :genetic_markers,        :delta
    remove_column :ncbi_gene_types,        :delta
    remove_column :ncbi_aliases,           :delta
    remove_column :ncbi_lineages,          :delta
    remove_column :ncbi_genomic_contexts,  :delta
    remove_column :ncbi_pubmed_articles,   :delta
  end
end

class CreateEthnicGroupings < ActiveRecord::Migration
  def change
    create_table :ethnic_groupings do |t|
      t.integer :gene_id
      t.integer :ethnic_group_id
      t.boolean :delta, :default => true, :null => false

      t.timestamps
    end
    add_index :ethnic_groupings, :gene_id
    add_index :ethnic_groupings, :ethnic_group_id
  end
end

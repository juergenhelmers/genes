class CreateCurabilities < ActiveRecord::Migration
  def change
    create_table :curabilities do |t|
      t.string :value
      t.boolean :delta, :default => true, :null => false


      t.timestamps
    end
    add_index :curabilities, :id,     :unique => true
    add_index :curabilities, :value,  :unique => true
  end
end

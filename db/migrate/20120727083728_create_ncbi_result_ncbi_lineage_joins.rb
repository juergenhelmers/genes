class CreateNcbiResultNcbiLineageJoins < ActiveRecord::Migration
  def change
    create_table :ncbi_result_ncbi_lineage_joins do |t|
      t.integer :ncbi_result_id
      t.integer :ncbi_lineage_id

      t.timestamps
    end
    add_index :ncbi_result_ncbi_lineage_joins, :ncbi_lineage_id, :name => 'index_result_lineage_linegae_id'
    add_index :ncbi_result_ncbi_lineage_joins, :ncbi_result_id, :name => 'index_result_lineage_result_id'
  end
end

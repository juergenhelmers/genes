class AddReviewToNcbiPubmedArticle < ActiveRecord::Migration
  def change
    add_column :ncbi_pubmed_articles, :review, :boolean, default: false
  end 
end

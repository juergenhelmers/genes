class AddDiseaseToGenes < ActiveRecord::Migration
  def change
    add_column :genes, :disease, :string
    add_index :genes, :disease
  end
end

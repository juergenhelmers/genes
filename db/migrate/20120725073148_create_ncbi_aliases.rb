class CreateNcbiAliases < ActiveRecord::Migration
  def change
    create_table :ncbi_aliases do |t|
      t.string :name

      t.timestamps
    end
    add_index :ncbi_aliases, :id,   :unique => true
    add_index :ncbi_aliases, :name
  end
end

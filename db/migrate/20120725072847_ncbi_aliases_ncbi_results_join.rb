class NcbiAliasesNcbiResultsJoin < ActiveRecord::Migration
  def change
      create_table :ncbi_aliases_ncbi_results, :id => false do |t|
        t.integer :ncbi_alias_id
        t.integer :ncbi_result_id
      end

      add_index :ncbi_aliases_ncbi_results, :ncbi_alias_id
      add_index :ncbi_aliases_ncbi_results, :ncbi_result_id
    end
end

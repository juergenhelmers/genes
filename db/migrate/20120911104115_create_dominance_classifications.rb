class CreateDominanceClassifications < ActiveRecord::Migration
  def change
    create_table :dominance_classifications do |t|
      t.integer :gene_id
      t.integer :dominance_id
      t.boolean :delta
      t.integer :user_id

      t.timestamps
    end
    add_index :dominance_classifications, :gene_id
    add_index :dominance_classifications, :dominance_id
  end
end

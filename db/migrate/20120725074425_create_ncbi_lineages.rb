class CreateNcbiLineages < ActiveRecord::Migration
  def change
    create_table :ncbi_lineages do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
    add_index :ncbi_lineages, :id,        :unique => true
    add_index :ncbi_lineages, :name
  end
end

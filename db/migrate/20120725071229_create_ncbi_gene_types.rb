class CreateNcbiGeneTypes < ActiveRecord::Migration
  def change
    create_table :ncbi_gene_types do |t|
      t.string :gene_type_name
      t.text :gene_type_description

      t.timestamps
    end
    add_index :ncbi_gene_types, :id, :unique => true
    add_index :ncbi_gene_types, :gene_type_name
  end
end

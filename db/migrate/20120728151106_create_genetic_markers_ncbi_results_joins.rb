class CreateGeneticMarkersNcbiResultsJoins < ActiveRecord::Migration
  def change
    create_table :genetic_markers_ncbi_results_joins do |t|
      t.integer :genetic_marker_id
      t.integer :ncbi_result_id

      t.timestamps
    end
    add_index :genetic_markers_ncbi_results_joins, :ncbi_result_id, :name => 'index_result_genmark_result_id'
    add_index :genetic_markers_ncbi_results_joins, :genetic_marker_id, :name => 'index_result_genmark_genmark_id'
  end
end

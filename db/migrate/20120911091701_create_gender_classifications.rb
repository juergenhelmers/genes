class CreateGenderClassifications < ActiveRecord::Migration
  def change
    create_table :gender_classifications do |t|
      t.integer :gene_id
      t.integer :gender_id
      t.boolean :delta
      t.integer :user_id

      t.timestamps
    end
  end
end

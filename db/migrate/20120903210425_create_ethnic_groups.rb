class CreateEthnicGroups < ActiveRecord::Migration
  def change
    create_table :ethnic_groups do |t|
      t.string :value
      t.boolean :delta, :default => true, :null => false

      t.timestamps
    end
    add_index :ethnic_groups, :id,      :unique => true
    add_index :ethnic_groups, :value,   :unique => true
  end
end

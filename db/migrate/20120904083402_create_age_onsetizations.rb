class CreateAgeOnsetizations < ActiveRecord::Migration
  def change
    create_table :age_onsetizations do |t|
      t.integer :gene_id
      t.integer :age_of_onset_id
      t.boolean :delta, :default => true, :null => false

      t.timestamps
    end
    add_index :age_onsetizations, :gene_id
    add_index :age_onsetizations, :age_of_onset_id
  end
end

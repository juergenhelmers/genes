class NcbiLineagesNcbiResultsJoin < ActiveRecord::Migration
  def change
    create_table :ncbi_lineages_ncbi_results, :id => false do |t|
      t.integer :ncbi_lineage_id
      t.integer :ncbi_result_id
    end

    add_index :ncbi_lineages_ncbi_results, :ncbi_lineage_id
    add_index :ncbi_lineages_ncbi_results, :ncbi_result_id
  end
end

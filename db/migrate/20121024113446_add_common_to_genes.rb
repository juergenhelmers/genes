class AddCommonToGenes < ActiveRecord::Migration
  def change
    add_column :genes, :common_in_sa, :boolean, :default => false
  end
end

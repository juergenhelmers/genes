class CreateZygosityClassifications < ActiveRecord::Migration
  def change
    create_table :zygosity_classifications do |t|
      t.integer :gene_id
      t.integer :zygosity_id
      t.boolean :delta
      t.integer :user_id

      t.timestamps
    end
    add_index :zygosity_classifications, :zygosity_id
    add_index :zygosity_classifications, :gene_id
  end
end

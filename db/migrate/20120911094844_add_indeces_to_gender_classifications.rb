class AddIndecesToGenderClassifications < ActiveRecord::Migration
  def change
    add_index :genders, :id
    add_index :gender_classifications, :gender_id
    add_index :gender_classifications, :gene_id
  end
end

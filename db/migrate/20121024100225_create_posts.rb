class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.text :content
      t.integer :user_id
      t.boolean :published

      t.timestamps
    end
    add_index :posts, :id,                   :unique => true
  end
end

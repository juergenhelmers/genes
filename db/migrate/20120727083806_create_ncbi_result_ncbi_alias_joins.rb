class CreateNcbiResultNcbiAliasJoins < ActiveRecord::Migration
  def change
    create_table :ncbi_result_ncbi_alias_joins do |t|
      t.integer :ncbi_result_id
      t.integer :ncbi_alias_id

      t.timestamps
    end
    add_index :ncbi_result_ncbi_alias_joins, :ncbi_result_id, :name => 'index_result_alias_result_id'
    add_index :ncbi_result_ncbi_alias_joins, :ncbi_alias_id, :name => 'index_result_alias_alias_id'
  end
end

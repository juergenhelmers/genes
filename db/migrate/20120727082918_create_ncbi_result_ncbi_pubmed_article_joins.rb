class CreateNcbiResultNcbiPubmedArticleJoins < ActiveRecord::Migration
  def change
    create_table :ncbi_result_ncbi_pubmed_article_joins do |t|
      t.integer :ncbi_result_id
      t.integer :ncbi_pubmed_article_id

      t.timestamps
    end
    add_index :ncbi_result_ncbi_pubmed_article_joins, :ncbi_result_id, :name => 'index_result_pubmed_result_ids'
    add_index :ncbi_result_ncbi_pubmed_article_joins, :ncbi_pubmed_article_id, :name => 'index_result_pubmed_article_ids'
  end
end

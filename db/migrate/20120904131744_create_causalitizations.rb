class CreateCausalitizations < ActiveRecord::Migration
  def change
    create_table :causalitizations do |t|
      t.integer :gene_id
      t.integer :causality_id
      t.boolean :delta, :default => true, :null => false

      t.timestamps
    end
    add_index :causalitizations, :gene_id
    add_index :causalitizations, :causality_id
  end
end

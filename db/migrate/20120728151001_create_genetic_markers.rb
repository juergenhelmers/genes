class CreateGeneticMarkers < ActiveRecord::Migration
  def change
    create_table :genetic_markers do |t|
      t.integer :unists_id
      t.string :name

      t.timestamps
    end
    add_index :genetic_markers, :id,   :unique => true
    add_index :genetic_markers, :name
  end
end

class CreateCausalities < ActiveRecord::Migration
  def change
    create_table :causalities do |t|
      t.string :name
	

      t.timestamps
    end
    add_index :causalities, :id, :unique => true
    add_index :causalities, :name
  end
end

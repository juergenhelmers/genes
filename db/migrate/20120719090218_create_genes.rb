class CreateGenes < ActiveRecord::Migration
  def change
    create_table :genes do |t|
      t.string :name
      t.string :chromosome
      t.integer :number_amplicons
      t.integer :total_bases
      t.integer :covered_bases
      t.float :overall_coverage

      t.timestamps
    end
    add_index :genes, :id,                   :unique => true
    add_index :genes, :name,                 :unique => true
    add_index :genes, :chromosome
  end
end

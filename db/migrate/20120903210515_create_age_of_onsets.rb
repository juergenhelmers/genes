class CreateAgeOfOnsets < ActiveRecord::Migration
  def change
    create_table :age_of_onsets do |t|
      t.string :value
      t.boolean :delta, :default => true, :null => false

      t.timestamps
    end
    add_index :age_of_onsets, :id,      :unique => true
    add_index :age_of_onsets, :value,   :unique => true
  end
end

class CreateChromosomes < ActiveRecord::Migration
  def change
    create_table :chromosomes do |t|
      t.string :name

      t.timestamps
    end
    add_index :chromosomes, :id
  end
end

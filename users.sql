--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: users; Type: TABLE; Schema: public; Owner: labor; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    email character varying(255) NOT NULL,
    encrypted_password character varying(255) NOT NULL,
    reset_password_token character varying(255),
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying(255),
    last_sign_in_ip character varying(255),
    login character varying(255) NOT NULL,
    firstname character varying(255) NOT NULL,
    lastname character varying(255) NOT NULL,
    role_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    active boolean DEFAULT false NOT NULL
);


ALTER TABLE public.users OWNER TO labor;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: labor
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO labor;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: labor
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: labor
--

SELECT pg_catalog.setval('users_id_seq', 8, true);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: labor
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: labor
--

COPY users (id, email, encrypted_password, reset_password_token, reset_password_sent_at, remember_created_at, sign_in_count, current_sign_in_at, last_sign_in_at, current_sign_in_ip, last_sign_in_ip, login, firstname, lastname, role_id, created_at, updated_at, active) FROM stdin;
5	pauljagnew@yahoo.com	$2a$10$9v7QpnNhXXT4VOPCcK7vjOyASZJXeu16/KS/2MF.fZjNukoESPhSm	\N	\N	\N	0	\N	\N	\N	\N	paul	Paul	Agnew	1	2012-08-30 16:48:51.413816	2012-10-25 07:28:41.28652	t
8	marco.weinberg@gmail.com	$2a$10$4BLHrh.HJtIBoCoGE/rxY.Jc8NRJdGm79FkJDbMe6MdQ8FuVhZywu	\N	\N	\N	3	2012-09-08 20:23:10.057419	2012-09-08 16:43:54.771383	176.67.87.16	75.83.193.162	marco	Marco	Weinberg	3	2012-08-30 16:56:50.162026	2012-10-25 07:28:41.300655	t
7	mhlanga@gmail.com	$2a$10$wBbBcDoq1Mj..I3jIwjdZOvxHx870jFpLtSPqoISk3VRfZqPLqnkG	\N	\N	\N	5	2012-10-12 05:48:28.01932	2012-09-26 16:20:36.053465	196.210.159.137	147.142.125.33	musa	Musa	Mhlanga	3	2012-08-30 16:55:00.613639	2012-10-25 07:28:41.307274	t
4	davedev87@gmail.com	$2a$10$VHJn6aqZMAPvLoTOBnEL7OmRvfrJl8m1M/pRmvAIIP49Rnzds7rJ2	\N	\N	\N	8	2012-10-05 06:45:08.268932	2012-09-26 07:15:52.998107	196.7.68.25	196.7.68.25	david	David	de Veredicis	1	2012-08-30 16:45:26.311942	2012-10-25 07:29:08.990059	f
6	neenadavies@yahoo.com	$2a$10$la9nz6AUbBSe3d1gCw2KWuqW9gcIt6mFaFUtC6SUKrDgir8gFsY2y	\N	\N	\N	2	2012-11-07 12:41:55.453187	2012-11-06 12:38:05.779186	74.68.121.69	74.68.121.69	neena	Neena	Davies	1	2012-08-30 16:52:27.498028	2012-11-07 12:41:55.455053	t
1	juergen.helmers@gmail.com	$2a$10$F1B1NRpXG8pYX5YGqHk/9.pMuCwucjPIHdWigBuSSrRZzGtdhQHz6	\N	\N	\N	44	2012-11-15 10:52:32.150091	2012-11-15 10:06:00.312401	146.64.8.10	146.64.8.10	helmerj	Juergen	Helmers	2	2012-08-29 13:33:53	2012-11-15 10:52:32.151506	t
\.


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: labor; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: index_users_on_active; Type: INDEX; Schema: public; Owner: labor; Tablespace: 
--

CREATE INDEX index_users_on_active ON users USING btree (active);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: labor; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_email ON users USING btree (email);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: labor; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON users USING btree (reset_password_token);


--
-- PostgreSQL database dump complete
--


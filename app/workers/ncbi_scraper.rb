class NcbiScraper
  require 'nokogiri'
  require 'bio'


  Bio::NCBI.default_email = "juergen.helmers@gmail.com"   # set bioruby default email address for requests to NCBI

  @queue = :ncbi_queue

  def self.query_ncbi_gene_id(gene_name,chromosome_number)
      ncbi_gene_id = Bio::NCBI::REST.esearch("#{gene_name}[gene name]+#{chromosome_number}[chromosome]+homo sapiens[organism]", {"db" => "gene"})[0]
      return ncbi_gene_id
    end

    def self.generate_aliases(aliases, result_id)
      result = NcbiResult.find(result_id)
      alias_ids = []
      aliases.each do |a|
        ncbi_alias = result.ncbi_aliases.find_or_create_by_name(a.text.lstrip)
        alias_ids << ncbi_alias.id
      end
      result = NcbiResult.find(result_id)
      result.ncbi_alias_ids = alias_ids
    end

    def self.generate_lineages(lineages, result_id)
      result = NcbiResult.find(result_id)
      lineages_ids = []                                                             # instantiate an empty array to hold ncbi_lineage_ids
      lineages.each do |lineage|                                                    # loop over lineage_names
        ncbi_lineage = result.ncbi_lineages.find_or_create_by_name(lineage.lstrip)  # find or create lineage with name
        lineages_ids << ncbi_lineage.id                                             # save lineage id in array
      end
      result = NcbiResult.find(result_id)
      result.ncbi_lineage_ids = lineages_ids                                        # associate the lineages with the result object
    end

    def self.query_refseq_status(gene_commentaries, result)
      gene_commentaries.each do |comment|
        if comment.xpath('Gene-commentary_heading').text == "RefSeq Status"
          result.refseq_status = comment.xpath('Gene-commentary_label').text.lstrip
        end
      end
    end

    def self.generate_related_links(references, result)
      references.each do |reference|
        reference_type = reference.xpath('Dbtag_db').text.lstrip
        #reference_id = reference.xpath('Dbtag_tag/Object-id/Object-id_id').text.lstrip
        reference_id = reference.xpath('Dbtag_tag/Object-id').text.lstrip.gsub("\n","")
        #Rails.logger.debug "RL #{reference_type}:#{reference_id}"
        result.ncbi_related_links.create!(:related_link_type => reference_type, :related_link_id => reference_id)
      end
    end

    def self.generate_ncbi_pubmed_articles(pubmed_reference_ids, result_id)
      pubmed_reference_id_hash = {}
      pubmed_reference_ids.each_with_index do |id, index|
        pubmed_reference_id_hash[index] = id.text
      end
      options = {
        "pubmed_reference_id_hash" => pubmed_reference_id_hash,
        "result_id" => result_id
      }
      Resque.enqueue(PubmedScraper, options)   # enqueue the has of ids to the pubmed resque worker

    end

    def self.parse_ncbi_dates(doc,type)
      date = doc.xpath("//Entrezgene_track-info/Gene-track/Gene-track_#{type}-date/Date/Date_std/Date-std")
      year = date.xpath('Date-std_year').text
      month = date.xpath('Date-std_month').text
      day = date.xpath('Date-std_day').text
      ncbi_date = "#{year}-#{month}-#{day}".to_date
      return ncbi_date
    end

    def self.generate_genentic_markers(doc, result_id)
      genetic_marker_ids = []
      result = NcbiResult.find(result_id)
      doc.xpath('//Gene-commentary/Gene-commentary_source/Other-source').each do |i|
        if i.xpath('Other-source_src/Dbtag/Dbtag_db').text == "UniSTS"
          unists = i.xpath('Other-source_src/Dbtag/Dbtag_tag/Object-id/Object-id_id').text
          genetic_marker = result.genetic_markers.find_or_create_by_unists_id(unists)
          genetic_marker.name = "#{i.xpath('Other-source_anchor').text} #{i.xpath('Other-source_post-text').text}"
          genetic_marker.save
          genetic_marker_ids << genetic_marker.id
        end
      end
      result = NcbiResult.find(result.id)
      result.genetic_marker_ids = genetic_marker_ids
    end



    def self.perform(gene_ids_hash)
      gene_ids_hash.each do |key,gene_id|
        gene = Gene.find(gene_id)                                                         # get the Gene object to work on
        gene_name = gene.name                                                             # assign gene name
        chromosome_number = gene.chromosome.gsub("chr", "")                               # assign the chromosome name
        ncbi_gene_id = query_ncbi_gene_id(gene_name,chromosome_number)                    # query the ncbi_gene_id from NCBI
        xml = Bio::NCBI::REST.efetch("#{ncbi_gene_id}", {"db"=>"gene","retmode"=>"xml"})  # get the gene record in XML from NCBI
        doc = Nokogiri::XML(xml)                                                          # create alias parsable nokogiri object from the xml
        result = NcbiResult.find_or_create_by_ncbi_gene_id(ncbi_gene_id)
        ncbi_update_at = parse_ncbi_dates(doc,type="update")

        if result.ncbi_updated_at == nil || result.ncbi_updated_at != ncbi_update_at
          result.gene_id = gene.id
          result.ncbi_created_at = parse_ncbi_dates(doc,type="create")
          result.ncbi_updated_at = ncbi_update_at
          ncbi_gene_type = NcbiGeneType.find_or_create_by_gene_type_name(doc.xpath('//Entrezgene_type').first.values[0])
          result.ncbi_gene_type_id = ncbi_gene_type.id
          result.summary = doc.xpath('//Entrezgene_summary').text
          result.official_symbol = doc.xpath('//Entrezgene_properties/Gene-commentary/Gene-commentary_properties/Gene-commentary/Gene-commentary_text').first.text
          result.official_full_name = doc.xpath('//Entrezgene_properties/Gene-commentary/Gene-commentary_properties/Gene-commentary/Gene-commentary_text')[1].text
          result.save
          # set lineages and create if not exist
          lineages = doc.xpath('Entrezgene-Set/Entrezgene/Entrezgene_source/BioSource/BioSource_org/Org-ref/Org-ref_orgname/OrgName/OrgName_lineage').text.split(";")
          generate_lineages(lineages, result.id)
          # set aliases and create if not exist
          aliases = doc.xpath('//Entrezgene_gene/Gene-ref/Gene-ref_syn/Gene-ref_syn_E')
          generate_aliases(aliases, result.id)
          # set RefSeq status
          gene_commentaries = doc.xpath('//Entrezgene_comments/Gene-commentary')
          query_refseq_status(gene_commentaries, result)
          # set reference links
          references = doc.xpath('//Entrezgene_gene/Gene-ref/Gene-ref_db/Dbtag')
          generate_related_links(references, result)
          # set genomic context
          location = doc.xpath('//Gene-ref_maploc').text.lstrip
          sequence = "Chromosome: #{chromosome_number}; #{doc.xpath('//Entrezgene_locus/Gene-commentary[1]/Gene-commentary_accession').text} (#{doc.xpath('//Entrezgene_locus/Gene-commentary[1]/Gene-commentary_seqs/Seq-loc/Seq-loc_int/Seq-interval/Seq-interval_from').text}..#{doc.xpath('//Entrezgene_locus/Gene-commentary[1]/Gene-commentary_seqs/Seq-loc/Seq-loc_int/Seq-interval/Seq-interval_to').text})"
          epigenomics_url = "http://www.ncbi.nlm.nih.gov/epigenomics/view/genome/#{ncbi_gene_id}?term=#{gene_name}"
          mapviewer_url = "http://www.ncbi.nlm.nih.gov/mapview/map_search.cgi?direct=on&idtype=gene&id=#{ncbi_gene_id}"
          result.create_ncbi_genomic_context(:location => location, :sequence => sequence, :epigenomics_url => epigenomics_url, :mapviewer_url => mapviewer_url)
          generate_genentic_markers(doc, result.id)


          pubmed_reference_ids = doc.xpath('//Entrezgene_comments/Gene-commentary/Gene-commentary_refs/Pub/Pub_pmid/PubMedId')
          generate_ncbi_pubmed_articles(pubmed_reference_ids, result.id)
        end


      end


    end

end
class NcbiArticleUpdateScraper
  require 'bio'

  Bio::NCBI.default_email = "juergen.helmers@gmail.com"       # set bioruby default email address for requests to NCBI

  @queue = :ncbi_queue                                        # specify the resque queue


  def self.perform(article_ids_hash)
    article_ids_hash.each do |key,article_id|
      ThinkingSphinx.deltas_enabled = false                   # disable delta indexing during update
      article = NcbiPubmedArticle.find(article_id)
      entry = Bio::PubMed.efetch(article.pubmed)[0]           # creates Bio::PubMed object from pubmed_id
      medline = Bio::MEDLINE.new(entry)                       # creates Bio::MEDLINE object from entry text
      article.review = medline.pubmed['PT'].include? "Review"
      article.save
    end

  end


end

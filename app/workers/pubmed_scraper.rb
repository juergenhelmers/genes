class PubmedScraper
  require 'bio'

  @queue = :pubmed_queue

  def self.perform(options)
    pubmed_reference_id_hash = options['pubmed_reference_id_hash']
    result = NcbiResult.find(options['result_id'])
    article_ids = []
    pubmed_reference_id_hash.each do |key, pubmed_id|
      article = NcbiPubmedArticle.find_by_pubmed(pubmed_id) || []
      if article.blank?
        # if article is not found create one by getting the article online record, parse and save it.
        entry = Bio::PubMed.efetch(pubmed_id)[0] # creates Bio::PubMed object from pubmed_id
        medline = Bio::MEDLINE.new(entry) # creates Bio::MEDLINE object from entry text
        is_review = medline.pubmed['PT'].include? "Review"
        reference = medline.reference # converts into Bio::Reference object
        article = result.ncbi_pubmed_articles.create!(:pubmed => pubmed_id, :authors => reference.authors.join(", "),  :title => reference.title,
          :journal => reference.journal, :volume => reference.volume,  :issue => reference.issue, :pages => reference.pages,
          :year => reference.year, :medline => reference.medline, :doi => reference.doi, :abstract => reference.abstract, :url => reference.url,
          :mesh => reference.mesh.join(", "), :embl_gb_record_number => reference.embl_gb_record_number, :sequence_position => reference.sequence_position,
          :comments => reference.comments, :affiliations => reference.affiliations.join(", "), :review => is_review)
        article_ids << article.id
      else
        # if article already exists in db, connect it with ncbi result.
        article_ids << article.id
      end
    end
    result = NcbiResult.find(options['result_id'])
    result.ncbi_pubmed_article_ids = article_ids # associate the new article with the result object

  end
end
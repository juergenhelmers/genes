
$(function () {
    new Highcharts.Chart({
        chart: {
            renderTo:   'portal_chart',
            type:       'line'
            },
        subtitle:      {text: 'Samples Per Month'},
        title:   {text: 'Dr. med. Peters'},
        xAxis: {
            title: 'Month',
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
             },
        yAxis: {
          title: { text: '# Samples'}

        },
        series: [{
          name: "all patients",
          data: [1, 2, 5, 7, 3, 6, 33, 27, 45, 40, 45, 65]
        },
        {
          name: "female patients",
          data: [1, 1, 3, 3, 2, 0, 21, 7, 22, 30, 25, 25]
        },
        {
//          showInLegend: false
          name: "male patients",
          data: [0, 1, 2, 4, 1, 6, 12, 20, 23, 10, 20, 40]
        }]
        });
  });
class NcbiRelatedLink < ActiveRecord::Base
  belongs_to :ncbi_result


  attr_accessible :ncbi_result_id, :related_link_type, :related_link_id
end

class GeneticMarkersNcbiResultsJoin < ActiveRecord::Base

  belongs_to :genetic_marker
  belongs_to :ncbi_result

  attr_accessible :genetic_marker_id, :ncbi_result_id
end


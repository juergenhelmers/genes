class Dominance < ActiveRecord::Base
  attr_accessible :name


  has_many :dominance_classifications
  has_many :genes, :through => :dominance_classifications

  validates_presence_of :name
  validates_uniqueness_of :name
end

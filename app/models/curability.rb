class Curability < ActiveRecord::Base
  attr_accessible :value

  has_many :curabilitizations
  has_many :genes, :through => :curabilitizations

end

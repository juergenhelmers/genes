class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable :rememberable, :registerable
  devise :database_authenticatable, :recoverable, :trackable, :validatable
  acts_as_tagger
  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :login, :firstname, :lastname, :active


  belongs_to :role
  has_many :posts, :dependent => :destroy
  before_create :set_default_role

  validates_presence_of :login, :email, :firstname, :lastname
  validates_uniqueness_of :login, :email


  def active_for_authentication?
    super && active?
  end

  def inactive_message
    if !active
      :not_active
    else
      super # Use whatever other message
    end
  end

  def self.send_reset_password_instructions(attributes={})
    recoverable = find_or_initialize_with_errors(reset_password_keys, attributes, :not_found)
    if !recoverable.active?
      recoverable.errors[:base] << I18n.t("devise.failure.not_active")
    elsif recoverable.persisted?
      recoverable.send_reset_password_instructions
    end
    recoverable
  end



  private
  def set_default_role
    if self.role.nil?
      self.role = Role.find_by_name('user')
    end
  end



end

class NcbiResultNcbiLineageJoin < ActiveRecord::Base
  belongs_to :ncbi_result
  belongs_to :ncbi_lineage


  attr_accessible :ncbi_lineage_id, :ncbi_result_id
end

class Gender < ActiveRecord::Base
  attr_accessible :name

  has_many :gender_classifications
  has_many :genes, :through => :gender_classifications

  validates_presence_of :name

end

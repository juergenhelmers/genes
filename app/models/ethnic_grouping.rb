class EthnicGrouping < ActiveRecord::Base
  attr_accessible :ethnic_group_id, :gene_id, :delta

  belongs_to :gene
  belongs_to :ethnic_group

  after_save :set_gene_delta_flag

  private
  def set_gene_delta_flag
    gene.delta = true
    gene.save
  end

end

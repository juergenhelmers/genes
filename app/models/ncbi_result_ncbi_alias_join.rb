class NcbiResultNcbiAliasJoin < ActiveRecord::Base
  belongs_to :ncbi_result
  belongs_to :ncbi_alias
  attr_accessible :ncbi_alias_id, :ncbi_result_id
end

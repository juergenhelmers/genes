class GeneticMarker < ActiveRecord::Base
  has_many :genetic_markers_ncbi_results_joins
  has_many :ncbi_results, :through => :genetic_markers_ncbi_results_joins

  attr_accessible :name, :unists_id
end

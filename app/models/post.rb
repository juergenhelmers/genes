class Post < ActiveRecord::Base
  belongs_to :user
  attr_accessible :content, :published, :title, :user_id

  scope :published, where(published: true)
  scope :unpublished, where(published: false)

end

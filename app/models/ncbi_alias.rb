class NcbiAlias < ActiveRecord::Base
  has_many :ncbi_result_ncbi_alias_joins
  has_many :ncbi_results, :through => :ncbi_result_ncbi_alias_joins

  attr_accessible :name
end

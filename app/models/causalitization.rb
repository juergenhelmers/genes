class Causalitization < ActiveRecord::Base
  attr_accessible :causality_id, :delta, :gene_id

  belongs_to :gene
  belongs_to :causality

  after_save :set_gene_delta_flag

  private
  def set_gene_delta_flag
    gene.delta = true
    gene.save
  end

end

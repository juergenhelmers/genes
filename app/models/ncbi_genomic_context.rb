class NcbiGenomicContext < ActiveRecord::Base
  belongs_to :ncbi_result
  attr_accessible :epigenomics_url, :location, :mapviewer_url, :ncbi_result_id, :sequence
end

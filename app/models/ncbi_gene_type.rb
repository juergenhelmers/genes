class NcbiGeneType < ActiveRecord::Base
  has_many :ncbi_results
  attr_accessible :gene_type_description, :gene_type_name
end

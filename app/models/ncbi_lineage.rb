class NcbiLineage < ActiveRecord::Base

  has_many :ncbi_result_ncbi_lineage_joins
  has_many :ncbi_results, :through => :ncbi_result_ncbi_lineage_joins

  attr_accessible :description, :name
end

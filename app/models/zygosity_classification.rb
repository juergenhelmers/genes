class ZygosityClassification < ActiveRecord::Base
  attr_accessible :delta, :gene_id, :user_id, :zygosity_id


  belongs_to :gene
  belongs_to :zygosity

  after_save :set_gene_delta_flag

  private
  def set_gene_delta_flag
    gene.delta = true
    gene.save
  end
end

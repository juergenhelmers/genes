class EthnicGroup < ActiveRecord::Base

  has_many :ethnic_groupings
  has_many :genes, :through => :ethnic_groupings

  attr_accessible :gene_id, :value

  validates_presence_of :value

end

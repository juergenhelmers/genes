class ChromosomeClassification < ActiveRecord::Base
  attr_accessible :chromosome_id, :delta, :gene_id, :user_id

  belongs_to :gene
  belongs_to :chromosome

  after_save :set_gene_delta_flag

  private
  def set_gene_delta_flag
    gene.delta = true
    gene.save
  end
end

class NcbiResult < ActiveRecord::Base
  belongs_to :gene
  belongs_to :ncbi_gene_type
  has_and_belongs_to_many :ncbi_aliases
  has_and_belongs_to_many :ncbi_lineages
  has_many :ncbi_related_links
  has_one :ncbi_genomic_context
  has_many :ncbi_result_ncbi_pubmed_article_joins
  has_many :ncbi_pubmed_articles, :through => :ncbi_result_ncbi_pubmed_article_joins
  has_many :ncbi_result_ncbi_lineage_joins
  has_many :ncbi_lineages, :through => :ncbi_result_ncbi_lineage_joins
  has_many :ncbi_result_ncbi_alias_joins
  has_many :ncbi_aliases, :through => :ncbi_result_ncbi_alias_joins
  has_many :genetic_markers_ncbi_results_joins
  has_many :genetic_markers, :through => :genetic_markers_ncbi_results_joins

  attr_accessible :gene_types_id, :ncbi_updated_at, :official_full_name, :official_symbol, :organism, :primary_source_link, :refseq_status, :ncbi_pubmed_article_id


end

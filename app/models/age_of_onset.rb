class AgeOfOnset < ActiveRecord::Base
  attr_accessible :value

  has_many :age_onsetizations
  has_many :genes, :through => :age_onsetizations

end

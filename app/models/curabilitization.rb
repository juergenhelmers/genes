class Curabilitization < ActiveRecord::Base
  attr_accessible :curability_id, :gene_id, :delta

  belongs_to :gene
  belongs_to :curability

  after_save :set_gene_delta_flag

  private
  def set_gene_delta_flag
    gene.delta = true
    gene.save
  end

end

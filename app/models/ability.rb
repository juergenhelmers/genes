class Ability
  include CanCan::Ability

  def initialize(user)

    if user.role.name == "admin"      # define the admin role
     can :manage, :all                # admin can do everything on all controllers
    elsif user.role.name == "staff"   # define the staff role
      can :read, :all                 # staff can read all
      can :update, Gene               # staff can use the update action in the genes_controller
      can :update, User do |u|        # staff can update their own user record
        u == user
      end
    elsif user.role.name == "user"    # define the user role
     can :read, :all                  # all other user can read everything
     can :update, User do |u|         # they can update their own user record
       u == user
     end
    else
      cannot :read, :all
    end
  end
end

class Zygosity < ActiveRecord::Base
  attr_accessible :name


  has_many :zygosity_classifications
  has_many :genes, :through => :zygosity_classifications

  validates_presence_of :name
end

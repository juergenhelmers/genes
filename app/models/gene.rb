class Gene < ActiveRecord::Base
  attr_accessible :disease,:common_in_sa, :chromosome, :covered_bases, :ncbi_result_id, :name, :number_amplicons, :overall_coverage, :total_bases, :ethnic_group_ids, :curability_ids, :age_of_onset_ids, :causality_ids,:comment_tags, :incident_tags, :gender_ids, :zygosity_ids, :chromosome_ids, :dominance_ids
  has_one :ncbi_result, :dependent => :destroy

  has_many :ethnic_groupings
  has_many :ethnic_groups, :through => :ethnic_groupings

  has_many :curabilitizations
  has_many :curabilities, :through => :curabilitizations

  has_many :age_onsetizations
  has_many :age_of_onsets, :through => :age_onsetizations

  has_many :causalitizations
  has_many :causalities, :through => :causalitizations

  has_many :gender_classifications
  has_many :genders, :through => :gender_classifications

  has_many :zygosity_classifications
  has_many :zygosities, :through => :zygosity_classifications

  has_many :chromosome_classifications
  has_many :chromosomes, :through => :chromosome_classifications

  has_many :dominance_classifications
  has_many :dominances, :through => :dominance_classifications

  # scopes
  scope :without_classification, where("id NOT IN (SELECT gene_id FROM ethnic_groupings)").where("id NOT IN (SELECT gene_id FROM curabilitizations)").where("id NOT IN (SELECT gene_id FROM age_onsetizations)").where("id NOT IN (SELECT gene_id FROM gender_classifications)").where("id NOT IN (SELECT gene_id FROM zygosity_classifications)").where("id NOT IN (SELECT gene_id FROM chromosome_classifications)").where("id NOT IN (SELECT gene_id FROM dominance_classifications)")
  scope :with_classification,    where( "id IN (SELECT gene_id FROM ethnic_groupings) OR id IN (SELECT gene_id FROM curabilitizations) OR id IN (SELECT gene_id FROM age_onsetizations) OR id IN (SELECT gene_id FROM gender_classifications) OR id IN (SELECT gene_id FROM zygosity_classifications) OR id IN (SELECT gene_id FROM chromosome_classifications) OR id IN (SELECT gene_id FROM dominance_classifications)")
  scope :common_in_sa,           where(:common_in_sa => true)


  acts_as_taggable
  acts_as_taggable_on :comments, :incidents

  attr_accessor :comment_tags, :incident_tags


  #create virtual attribute for file name
  attr_accessor :mutation_annotation_file


  validates_presence_of :name, :chromosome
  validates_uniqueness_of :name

  define_index do
    indexes :name
    indexes chromosome
    indexes disease

    # add classification records
    indexes curabilities.value, as: :gene_curabilities
    indexes ethnic_groups.value, as: :gene_ethnic_groups
    indexes age_of_onsets.value, as: :gene_age_of_onsets
    indexes causalities.name, as: :gene_causalities
    indexes genders.name, as: :gene_genders
    indexes zygosities.name, as: :gene_zygosities
    indexes chromosomes.name, as: :gene_chromosomes
    indexes dominances.name, as: :gene_dominances

    indexes ncbi_result.official_full_name, as: :result_official_full_name
    indexes ncbi_result.phenotype, as: :result_phenotype
    indexes ncbi_result.summary, as: :result_summary
    #indexes ncbi_result.ncbi_gene_type.gene_type_name, as: :result_gene_type_name
    indexes ncbi_result.ncbi_aliases.name, as: :result_aliases_name
    #indexes ncbi_result.ncbi_lineages.name, as: :result_lineages_name

    #indexes ncbi_result.genetic_markers.name, as: :result_genetic_markers_name #long index time!
    #indexes ncbi_result.ncbi_genomic_context.location, as: :result_genomic_context_location
    #indexes ncbi_result.ncbi_pubmed_articles.title, as: :result_pubmed_title #long index time!
    #indexes ncbi_result.ncbi_pubmed_articles.authors, as: :result_pubmed_authors
    #indexes ncbi_result.ncbi_pubmed_articles.abstract, as: :result_pubmed_abstract
    #indexes ncbi_result.ncbi_pubmed_articles.volume, as: :result_pubmed_volume
    #indexes ncbi_result.ncbi_pubmed_articles.year, as: :result_pubmed_year
    #indexes ncbi_result.ncbi_pubmed_articles.affiliations, as: :result_pubmed_affiliations

    indexes comments.name, as: :comments
    indexes incidents.name, as: :incidents

    set_property :delta => true

  end
  #  thinking_sphinx_scopes
  #sphinx_scope(:classified) {
  #
  #  where( "id IN (SELECT gene_id FROM ethnic_groupings) OR id IN (SELECT gene_id FROM curabilitizations) OR id IN (SELECT gene_id FROM age_onsetizations) OR id IN (SELECT gene_id FROM gender_classifications) OR id IN (SELECT gene_id FROM zygosity_classifications) OR id IN (SELECT gene_id FROM chromosome_classifications) OR id IN (SELECT gene_id FROM dominance_classifications)")
  #
  #  }


  private
  def self.calculate_progress(unclassified, classified)
    if unclassified > 0 || classified > 0
      all = unclassified.to_i + classified.to_i
      u = (100.to_f / all.to_f * unclassified).to_i
      c = 100 - u
      progress = { 'classified' => "#{c}", 'unclassified' => "#{u}"}
    else
      progress = { 'classified' => 0, 'unclassified' => 100}
    end
    return progress
  end

  def self.generate_related_link(type,id)
    if type == "HGNC"
      return "http://www.genenames.org/data/hgnc_data.php?hgnc_id=#{id}"
    elsif type == "Ensembl"
      return "http://www.ensembl.org/id/#{id}"
    elsif type == "HPRD"
      return "http://www.hprd.org/protein/#{id}"
    elsif type == "MIM"
      return "http://www.ncbi.nlm.nih.gov/omim/#{id}"
    elsif type == "Vega"
      return "http://vega.sanger.ac.uk/id/#{id}"
    end
  end

end

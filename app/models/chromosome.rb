class Chromosome < ActiveRecord::Base
  attr_accessible :name


  has_many :chromosome_classifications
  has_many :genes, :through => :chromosome_classifications

  validates_presence_of :name
  validates_uniqueness_of :name
end

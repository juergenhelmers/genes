class AgeOnsetization < ActiveRecord::Base
  attr_accessible :age_of_onset_id, :delta, :gene_id

  belongs_to :gene
  belongs_to :age_of_onset

  after_save :set_gene_delta_flag

  private
  def set_gene_delta_flag
    gene.delta = true
    gene.save
  end

end

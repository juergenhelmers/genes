class Causality < ActiveRecord::Base
  attr_accessible :name

  has_many :causalitizations
  has_many :genes, :through => :causalitizations

end

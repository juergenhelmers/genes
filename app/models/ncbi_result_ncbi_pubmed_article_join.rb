class NcbiResultNcbiPubmedArticleJoin < ActiveRecord::Base
  belongs_to :ncbi_pubmed_article
  belongs_to :ncbi_result


  attr_accessible :ncbi_pubmed_article_id, :ncbi_result_id
end

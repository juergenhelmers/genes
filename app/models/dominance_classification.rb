class DominanceClassification < ActiveRecord::Base
  attr_accessible :delta, :dominance_id, :gene_id, :user_id

  belongs_to :gene
  belongs_to :dominance

  after_save :set_gene_delta_flag

  private
  def set_gene_delta_flag
    gene.delta = true
    gene.save
  end
end

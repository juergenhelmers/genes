class NcbiPubmedArticle < ActiveRecord::Base

  has_many :ncbi_result_ncbi_pubmed_article_joins
  has_many :ncbi_results, :through => :ncbi_result_ncbi_pubmed_article_joins

  attr_accessible :abstract, :affiliations, :authors, :comments, :doi, :embl_gb_record_number, :issue, :journal, :medline, :mesh, :pages, :pubmed, :sequence_position, :title, :url, :volume, :year
end

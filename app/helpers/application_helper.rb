module ApplicationHelper

  def admin?
    if current_user.role.name == "admin"
      return true
    else
      return false
    end
  end

  def staff?
    if current_user.role.name == "staff"
      return true
    else
      return false
    end
  end

end

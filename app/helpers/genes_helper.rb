module GenesHelper

  def show_author(article)
    authors = article.authors.split(",")
    if authors.count == 1
      return authors[0].gsub("---\n- ","")
    elsif authors.count == 2
      return "#{authors[0].gsub("---\n- ","")} and #{authors[1].gsub("--- - ","")}"
    else
      return "#{authors[0].gsub("---\n- ","")} et al."
    end
  end

  def generate_related_link(link)
    if link.related_link_type == "HGNC"
      return "http://www.genenames.org/data/hgnc_data.php?hgnc_id=#{link.related_link_id}"
    elsif link.related_link_type == "Ensembl"
      return "http://www.ensembl.org/id/#{link.related_link_id}"
    elsif link.related_link_type == "HPRD"
      return "http://www.hprd.org/protein/#{link.related_link_id}"
    elsif link.related_link_type == "MIM"
      return "http://www.ncbi.nlm.nih.gov/omim/#{link.related_link_id}"
    elsif link.related_link_type == "Vega"
      return "http://vega.sanger.ac.uk/id/#{link.related_link_id}"
    end
  end

  def classified_gene(gene,i)
    if gene.ethnic_groups.size > 0 || gene.curabilities.size > 0 || gene.age_of_onsets.size > 0  || gene.causalities.size > 0 || gene.incidents.present? || gene.comments.present? || gene.genders.size > 0 || gene.zygosities.size > 0 || gene.chromosomes.size > 0 || gene.dominances.size > 0
      if i == 1
        image_tag('true.png')
      else
        return true
      end
    else
      if i == 1
        image_tag('false.png')
      else
        return false
      end
    end
  end

end

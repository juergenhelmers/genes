class DominancesController < ApplicationController
  before_filter :authenticate_user!
  authorize_resource

  # GET /dominances
  # GET /dominances.json
  def index
    @dominances = Dominance.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @dominances }
    end
  end

  # GET /dominances/1
  # GET /dominances/1.json
  def show
    @dominance = Dominance.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @dominance }
    end
  end

  # GET /dominances/new
  # GET /dominances/new.json
  def new
    @dominance = Dominance.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @dominance }
    end
  end

  # GET /dominances/1/edit
  def edit
    @dominance = Dominance.find(params[:id])
  end

  # POST /dominances
  # POST /dominances.json
  def create
    @dominance = Dominance.new(params[:dominance])

    respond_to do |format|
      if @dominance.save
        format.html { redirect_to dominances_path, notice: 'Dominance was successfully created.' }
        format.json { render json: @dominance, status: :created, location: @dominance }
      else
        format.html { render action: "new" }
        format.json { render json: @dominance.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /dominances/1
  # PUT /dominances/1.json
  def update
    @dominance = Dominance.find(params[:id])

    respond_to do |format|
      if @dominance.update_attributes(params[:dominance])
        format.html { redirect_to @dominance, notice: 'Dominance was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @dominance.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dominances/1
  # DELETE /dominances/1.json
  def destroy
    @dominance = Dominance.find(params[:id])
    @dominance.destroy

    respond_to do |format|
      format.html { redirect_to dominances_url }
      format.json { head :no_content }
    end
  end
end

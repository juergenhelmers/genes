class UsersController < ApplicationController

  before_filter :authenticate_user!
  authorize_resource

  def index
    if admin?
      @users = User.order("login ASC")
    else
      @users = User.where(id: current_user.id)
    end


    respond_to do |format|
      format.html # index.html.haml
      format.json { render json: @users }
    end
  end
  
  # GET /:users/1
    # GET /:users/1.json
    def show
      if admin? || params[:id] == current_user.id
        @user = User.find(params[:id])
      else
        @user = User.find(current_user.id)
      end


      respond_to do |format|
        format.html # show.html.erb
        format.json { render json: @user }
      end
    end
  
    # GET /:users/new
    # GET /:users/new.json
    def new
      @user = User.new
  
      respond_to do |format|
        format.html # new.html.erb
        format.json { render json: @user }
      end
    end
  
    # GET /:users/1/edit
    def edit
      if admin? || params[:id] == current_user.id
        @user = User.find(params[:id])
      else
        @user = User.find(current_user.id)
      end

    end
  
    # POST /:users
    # POST /:users.json
  def create
    @user = User.new
    @user.accessible = [:role_id] if admin?
    @user.attributes = params[:user]

    respond_to do |format|
      if admin?
        if @user.save
          format.html { redirect_to @user, notice: 'User was successfully created.' }
          format.json { render json: @user, status: :created, location: @user }
        else
          format.html { redirect_to new_user_path }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      else
        redirect_to genes_path, error: "You are not authorized to create new users"
      end

    end
  end
  
    # PUT /:users/1
    # PUT /:users/1.json
    def update
      @user = User.find(params[:id])
      @user.accessible = [:role_id] if admin?

      if params[:user][:password].blank?
        params[:user].delete(:password)
        params[:user].delete(:password_confirmation)
      end

      respond_to do |format|
        if admin? || @user.id == current_user.id
          if @user.update_attributes(params[:user])
            format.html { redirect_to @user, notice: 'User was successfully updated.' }
            format.json { head :no_content }
          else
            format.html { render action: "edit" }
            format.json { render json: @user.errors, status: :unprocessable_entity }
          end
        else
          redirect_to genes_path, error: "You are not authorized to update this user!."
        end
      end
    end
  
    # DELETE /:users/1
    # DELETE /:users/1.json
    def destroy
      @user = User.find(params[:id])
      if admin?
        @user.destroy
      else
        redirect_to genes_path, error: "You are not authorized to delete this user!"
      end

  
      respond_to do |format|
        format.html { redirect_to :users }
        format.json { head :no_content }
      end
    end



end

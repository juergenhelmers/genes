class WelcomeController < ApplicationController
  skip_authorization_check
  def index


    @unclassified_genes = Gene.without_classification
    @classified_genes = Gene.with_classification
    @common_genes = Gene.common_in_sa
    @progress = Gene.calculate_progress(@unclassified_genes.size, @classified_genes.size)
    @progress_sa = Gene.calculate_progress(@common_genes.without_classification.size, @common_genes.with_classification.size)
    @posts = Post.published.order("created_at DESC").limit(2) || []
  end
end

class ZygositiesController < ApplicationController

  before_filter :authenticate_user!
  authorize_resource

  # GET /zygosities
  # GET /zygosities.json
  def index
    @zygosities = Zygosity.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @zygosities }
    end
  end

  # GET /zygosities/1
  # GET /zygosities/1.json
  def show
    @zygosity = Zygosity.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @zygosity }
    end
  end

  # GET /zygosities/new
  # GET /zygosities/new.json
  def new
    @zygosity = Zygosity.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @zygosity }
    end
  end

  # GET /zygosities/1/edit
  def edit
    @zygosity = Zygosity.find(params[:id])
  end

  # POST /zygosities
  # POST /zygosities.json
  def create
    @zygosity = Zygosity.new(params[:zygosity])

    respond_to do |format|
      if @zygosity.save
        format.html { redirect_to zygosities_path, notice: 'Zygosity was successfully created.' }
        format.json { render json: @zygosity, status: :created, location: @zygosity }
      else
        format.html { render action: "new" }
        format.json { render json: @zygosity.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /zygosities/1
  # PUT /zygosities/1.json
  def update
    @zygosity = Zygosity.find(params[:id])

    respond_to do |format|
      if @zygosity.update_attributes(params[:zygosity])
        format.html { redirect_to @zygosity, notice: 'Zygosity was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @zygosity.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /zygosities/1
  # DELETE /zygosities/1.json
  def destroy
    @zygosity = Zygosity.find(params[:id])
    @zygosity.destroy

    respond_to do |format|
      format.html { redirect_to zygosities_url }
      format.json { head :no_content }
    end
  end
end

class GendersController < ApplicationController
  before_filter :authenticate_user!
  authorize_resource

  # GET /genders
  # GET /genders.json
  before_filter :authenticate_user!
  before_filter do
    redirect_to :root, notice: "Admin access required." unless current_user && admin?
  end

  def index
    @genders = Gender.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @genders }
    end
  end

  # GET /genders/1
  # GET /genders/1.json
  def show
    @gender = Gender.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @gender }
    end
  end

  # GET /genders/new
  # GET /genders/new.json
  def new
    @gender = Gender.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @gender }
    end
  end

  # GET /genders/1/edit
  def edit
    @gender = Gender.find(params[:id])
  end

  # POST /genders
  # POST /genders.json
  def create
    @gender = Gender.new(params[:gender])

    respond_to do |format|
      if @gender.save
        format.html { redirect_to @gender, notice: 'Gender was successfully created.' }
        format.json { render json: @gender, status: :created, location: @gender }
      else
        format.html { redirect_to new_gender_path }
        format.json { render json: @gender.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /genders/1
  # PUT /genders/1.json
  def update
    @gender = Gender.find(params[:id])

    respond_to do |format|
      if @gender.update_attributes(params[:gender])
        format.html { redirect_to @gender, notice: 'Gender was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @gender.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /genders/1
  # DELETE /genders/1.json
  def destroy
    @gender = Gender.find(params[:id])
    @gender.destroy

    respond_to do |format|
      format.html { redirect_to genders_url }
      format.json { head :no_content }
    end
  end
end

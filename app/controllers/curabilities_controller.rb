class CurabilitiesController < ApplicationController
  before_filter :authenticate_user!
  authorize_resource

  # GET /curabilities
  # GET /curabilities.json
  def index
    @curabilities = Curability.order("value asc")

    respond_to do |format|
      format.html # index.html.haml
      format.json { render json: @curabilities }
    end
  end

  # GET /curabilities/1
  # GET /curabilities/1.json
  def show
    @curability = Curability.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @curability }
    end
  end

  # GET /curabilities/new
  # GET /curabilities/new.json
  def new
    @curability = Curability.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @curability }
    end
  end

  # GET /curabilities/1/edit
  def edit
    @curability = Curability.find(params[:id])
  end

  # POST /curabilities
  # POST /curabilities.json
  def create
    @curability = Curability.new(params[:curability])

    respond_to do |format|
      if @curability.save
        format.html { redirect_to @curability, notice: 'Curability was successfully created.' }
        format.json { render json: @curability, status: :created, location: @curability }
      else
        format.html { render action: "new" }
        format.json { render json: @curability.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /curabilities/1
  # PUT /curabilities/1.json
  def update
    @curability = Curability.find(params[:id])

    respond_to do |format|
      if @curability.update_attributes(params[:curability])
        format.html { redirect_to curabilities_path, notice: 'Curability was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @curability.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /curabilities/1
  # DELETE /curabilities/1.json
  def destroy
    @curability = Curability.find(params[:id])
    @curability.destroy



    respond_to do |format|
      format.html { redirect_to curabilities_url }
      format.json { head :no_content }
    end
  end
end

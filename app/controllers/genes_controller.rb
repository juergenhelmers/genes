class GenesController < ApplicationController

  before_filter :authenticate_user!
  authorize_resource
  # GET /genes
  # GET /genes.json

  require 'csv'

  def report
    @genes = Gene.where( common_in_sa: true )
  end

  def index
    if params[:search].present?
      #if params[:tag]
      #  @genes = Gene.tagged_with(params[:tag]).search(params[:search], page: params[:page], per_page: 20)
      #else
        @genes = Gene.includes(:ncbi_result,:incidents,:comments).search(params[:search], page: params[:page], per_page: 20)
      #end
    else
      if params[:filter]
        if params[:filter] == "common_in_sa"
          @genes = Gene.includes(:ncbi_result,:incidents,:comments).common_in_sa.order("name ASC").paginate(:page => params[:page], :per_page => 20)
        elsif params[:filter] == "classified"
          @genes = Gene.includes(:ncbi_result).with_classification.order("name ASC").paginate(:page => params[:page], :per_page => 20)
        elsif params[:filter] == "unclassified"
          @genes = Gene.includes(:ncbi_result,:incidents,:comments).without_classification.order("name ASC").paginate(:page => params[:page], :per_page => 20)
        end
      else
        @genes = Gene.includes(:ncbi_result,:incidents,:comments).order("name ASC").paginate(:page => params[:page], :per_page => 20)
      end
    end

    respond_to do |format|
      format.html # index.html.haml
      format.json { render json: @genes }
    end
  end

  # GET /genes/1
  # GET /genes/1.json
  def show
    @gene = Gene.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @gene }
    end
  end

  # GET /genes/new
  # GET /genes/new.json
  def new
    if current_user.role.name == "admin" || current_user.role.name == "staff"
      @gene = Gene.new
      respond_to do |format|
        format.html # new.html.erb
        format.json { render json: @gene }
      end
    else
      respond_to do |format|
        format.html {redirect_to genes_path, notice: "You are not authorized to create new records."}
        format.json { render json: @gene }
      end
    end
  end

  # GET /genes/1/edit
  def edit
    @gene = Gene.find(params[:id])
    @ethnic_groups = EthnicGroup.order("value asc")
    @curabilities = Curability.order("value asc")
    @age_of_onsets = AgeOfOnset.all
    @causalities = Causality.all
    @genders = Gender.order("name ASC")
    @zygosities = Zygosity.all
    @chromosomes = Chromosome.all
    @dominances = Dominance.all
  end

  # POST /genes
  # POST /genes.json
  def create
    # read input file as CSV
    input_file = CSV.read(params[:gene][:mutation_annotation_file].tempfile.path, :headers => true, :col_sep => ";")
    input_file.each do |line|
      if current_user.role.name == "admin"
        @gene = Gene.create!(:name => line['Name'], :chromosome => line['Chromosome'], :number_amplicons => line['Num_Amplicons'], :total_bases => line['Total_Bases'], :covered_bases => line['Covered_Bases'], :overall_coverage => line['Overall_Coverage'])
      end

    end
    gene_ids_hash = {}                          # instantiate an empty hash to hold id's of gene w/o a ncbi_result
    Gene.all.each_with_index do |gene,index|    # loop over all genes
      unless gene.ncbi_result.present?
        gene_ids_hash[index] = gene.id          # save gene ids of gene w/o ncbi_result into hash for ncbi query
      end
    end
    #NcbiWorker.perform_async(gene_ids_hash)     # call sidekiq worker to create ncbi_result record
    if current_user.role.name == "admin"
      Resque.enqueue(NcbiScraper, gene_ids_hash)   # enqueue the hash of ids to the resque worker
    end


    respond_to do |format|
      if current_user.role.name == "admin" && @gene.save
        format.html { redirect_to genes_path, notice: 'Genes were successfully created. Additional information is being gathered...' }
        format.json { render json: @gene, status: :created, location: @gene }
      else
        format.html { render action: "new" }
        format.json { render json: @gene.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /genes/1
  # PUT /genes/1.json
  def update
    @gene = Gene.find(params[:id])
    @ethnic_groups = EthnicGroup.order("value asc")
    @curabilities = Curability.order("value asc")
    @age_of_onsets = AgeOfOnset.all
    @causalities = Causality.all
    @genders = Gender.order("name ASC")
    @zygosities = Zygosity.all
    @chromosomes = Chromosome.all
    @dominances = Dominance.all
    respond_to do |format|
      if current_user.role.name == "admin" || current_user.role.name == "staff"
        @gene.update_attributes(params[:gene])
        current_user.tag(@gene, with: "#{params[:gene][:comment_tags]}", on: :comments)
        current_user.tag(@gene, with: "#{params[:gene][:incident_tags]}", on: :incidents)
        format.html { redirect_to @gene, notice: 'Gene was successfully updated.' }
        format.json { head :no_content }
      else
        @ethnic_groups = EthnicGroup.order("value asc")
        @curabilities = Curability.order("value asc")
        @age_of_onsets = AgeOfOnset.all
        @causalities = Causality.all
        @genders = Gender.order("name ASC")
        @zygosities = Zygosity.all
        @chromosomes = Chromosome.all
        @dominances = Dominance.all
        format.html { render action: "show", error: "You are not authorized to edit this record." }
        format.json { render json: @gene.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /genes/1
  # DELETE /genes/1.json
  def destroy
    @gene = Gene.find(params[:id])
    if current_user.role.name == "admin"
      @gene.destroy
    end


    respond_to do |format|
      format.html { redirect_to genes_url }
      format.json { head :no_content }
    end
  end

  protected


end

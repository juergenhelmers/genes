class AgeOfOnsetsController < ApplicationController
  before_filter :authenticate_user!
  authorize_resource

  # GET /age_of_onsets
  # GET /age_of_onsets.json
  def index
    @age_of_onsets = AgeOfOnset.all

    respond_to do |format|
      format.html # index.html.haml
      format.json { render json: @age_of_onsets }
    end
  end

  # GET /age_of_onsets/1
  # GET /age_of_onsets/1.json
  def show
    @age_of_onset = AgeOfOnset.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @age_of_onset }
    end
  end

  # GET /age_of_onsets/new
  # GET /age_of_onsets/new.json
  def new
    @age_of_onset = AgeOfOnset.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @age_of_onset }
    end
  end

  # GET /age_of_onsets/1/edit
  def edit
    @age_of_onset = AgeOfOnset.find(params[:id])
  end

  # POST /age_of_onsets
  # POST /age_of_onsets.json
  def create
    @age_of_onset = AgeOfOnset.new(params[:age_of_onset])

    respond_to do |format|
      if @age_of_onset.save
        format.html { redirect_to @age_of_onset, notice: 'Age of onset was successfully created.' }
        format.json { render json: @age_of_onset, status: :created, location: @age_of_onset }
      else
        format.html { render action: "new" }
        format.json { render json: @age_of_onset.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /age_of_onsets/1
  # PUT /age_of_onsets/1.json
  def update
    @age_of_onset = AgeOfOnset.find(params[:id])

    respond_to do |format|
      if @age_of_onset.update_attributes(params[:age_of_onset])
        format.html { redirect_to age_of_onsets_path, notice: 'Age of onset was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @age_of_onset.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /age_of_onsets/1
  # DELETE /age_of_onsets/1.json
  def destroy
    @age_of_onset = AgeOfOnset.find(params[:id])
    @age_of_onset.destroy


    respond_to do |format|
      format.html { redirect_to age_of_onsets_path }
      format.json { head :no_content }
    end
  end
end

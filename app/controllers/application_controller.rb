class ApplicationController < ActionController::Base
  check_authorization :unless => :devise_controller?

  protect_from_forgery

  rescue_from CanCan::AccessDenied do |exception|
    flash[:error] = exception.message
    redirect_to root_url
  end


  # functions to inquire about a user role
  def admin?
    current_user.role.name == "admin"
  end
  def staff?
    current_user.role.name == "staff"
  end
  def user?
    current_user.role.name == "user"
  end

end

class CausalitiesController < ApplicationController
  before_filter :authenticate_user!
  authorize_resource
  # GET /causalities
  # GET /causalities.json
  def index
    @causalities = Causality.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @causalities }
    end
  end

  # GET /causalities/1
  # GET /causalities/1.json
  def show
    @causality = Causality.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @causality }
    end
  end

  # GET /causalities/new
  # GET /causalities/new.json
  def new
    @causality = Causality.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @causality }
    end
  end

  # GET /causalities/1/edit
  def edit
    @causality = Causality.find(params[:id])
  end

  # POST /causalities
  # POST /causalities.json
  def create
    @causality = Causality.new(params[:causality])

    respond_to do |format|
      if @causality.save
        format.html { redirect_to @causality, notice: 'Causality was successfully created.' }
        format.json { render json: @causality, status: :created, location: @causality }
      else
        format.html { render action: "new" }
        format.json { render json: @causality.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /causalities/1
  # PUT /causalities/1.json
  def update
    @causality = Causality.find(params[:id])

    respond_to do |format|
      if @causality.update_attributes(params[:causality])
        format.html { redirect_to causalities_path, notice: 'Causality was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @causality.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /causalities/1
  # DELETE /causalities/1.json
  def destroy
    @causality = Causality.find(params[:id])
    @causality.destroy

    respond_to do |format|
      format.html { redirect_to causalities_url }
      format.json { head :no_content }
    end
  end
end

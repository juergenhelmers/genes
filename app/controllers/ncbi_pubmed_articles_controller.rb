class NcbiPubmedArticlesController < ApplicationController
  before_filter :authenticate_user!
  authorize_resource

def show
  @gene = Gene.find(params[:id])
  if params[:filter] == "review"
    @pubmed_articles = @gene.ncbi_result.ncbi_pubmed_articles.where(review: true).order("year DESC")
  else
    @pubmed_articles = @gene.ncbi_result.ncbi_pubmed_articles.order("year DESC")
  end
end

end
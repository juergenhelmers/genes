class PortalsController < ApplicationController
  before_filter :authenticate_user!
  authorize_resource

  def index
  end

  def medical
    @gene = Gene.find_by_name('PTPN11')
  end

  def science
    @gene = Gene.find_by_name('PTPN11')
  end

end

require 'factory_girl'

Given /^a user has logged in$/ do
  user = FactoryGirl.create(:user)
  visit('/my/users/sign_in')
  fill_in 'Email', with: user.email
  fill_in 'Password', with: user.password
  click_button "Sign in"
  page.should have_content("Signed in successfully.")
  current_path.should eq('/')
end

When /^I go to the home page$/ do
  visit root_url
end

Then /^I should see "(.*?)"$/ do |text|
  page.should have_content(text)
end

And /^I should not see "(.*?)"$/ do |text|
  page.should_not have_content(text)
end




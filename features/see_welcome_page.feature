Feature: Check for welcome page features

  Background: Log in as an existing user
    Given a user has logged in


  Scenario: Logged in User should see the menu for regular user
    Given I go to the home page
    Then I should see "Genes"
    And I should see "Portal"
    And I should not see "Admin"



